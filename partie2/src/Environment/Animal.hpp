/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include <utility>
#include "Environment.hpp"
#include "Positionable.hpp"
#include "Utility/Types.hpp"
#include "Animal.hpp"
#include "Application.hpp"




class Animal: public Positionable{

public:
    Animal(const Vec2d& pos, int pt_vie = 1, int esp_vie = (getAppConfig().animal_default_lifespan));
    double getSpeed() const;
    Angle getDirection() const ;
    bool isDead();
    void drawOn(sf::RenderTarget& target);
    void move(sf::Time const dt);
    RotationProbs computeRotationProbs();
    void update(sf::Time dt); //used to udate counter & comput rotation



private:
    Angle direction_;
    int pt_de_vie_;
    int life_expextancy_;
    sf::Time counter_since_last_rotation_;

protected:
    void setDirection(const Angle&); //seems like smth is wrong
};
