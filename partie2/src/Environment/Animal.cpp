/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

//#include "Animal.hpp"
//#include <utility> no need, is in .hpp
//#include "Random/RandomGenerator.hpp" don't think we need that
#include <SFML/Graphics.hpp>
#include "Utility/Utility.hpp"
#include "Animal.hpp"
#include "Utility/Constants.hpp"
#include "Random/Random.hpp"
#include "Random/RandomGenerator.hpp"


double Animal::getSpeed() const {
    return getAppConfig().animal_default_speed;
}

Angle Animal::getDirection() const {
    return direction_;
}

void Animal::setDirection(const Angle& alpha){
    direction_ = alpha;
}

bool Animal::isDead(){
    return ((pt_de_vie_<=0) or (life_expextancy_<=0));
}

void Animal::drawOn(sf::RenderTarget& target){

    auto const animal_sprite = buildSprite((getPosition().toVec2d()),/*size: where do i find that --> 20.0 is completely arbitrary? */ 50.0 ,getAppTexture(getAppConfig().animal_default_texture), direction_/DEG_TO_RAD);
    //need to find a way to rotate the sprite, tried using rotate (supposed to be in sf) but not found and sprite is const so no luck there
    //reference: drawOn in Food:)
    //animal_sprite.setRotation(direction_);//does not work bc sprite is const
    target.draw(animal_sprite);
    if (isDebugOn()){
        auto const text = buildText(to_nice_string(pt_de_vie_), ((getPosition().toVec2d()) + Vec2d(0, 25)), getAppFont(), 20, sf::Color::Red);
        target.draw(text);
        sf::VertexArray ligne(sf::PrimitiveType::Lines, 2);
        ligne[0]={(getPosition().toVec2d()), sf::Color::Black};//need to find a way to code arrival coord
        ligne[1]={((getPosition().toVec2d())+((Vec2d::fromAngle(getDirection())) * 60.0) /*arbitrary*/), sf::Color::Black};
        target.draw(ligne);
    }
}

Animal::Animal(const Vec2d& pos, int pt_vie, int esp_vie)
    : Positionable(ToricPosition(pos)), direction_(uniform(0.0, TAU)), pt_de_vie_(pt_vie), life_expextancy_(esp_vie),
      counter_since_last_rotation_(sf::Time::Zero)
{}

void Animal::move(sf::Time dt){

    auto dx = getSpeed()*(Vec2d::fromAngle(getDirection())) * dt.asSeconds();
    setPosition(getPosition() + ToricPosition(dx));
}

RotationProbs Animal::computeRotationProbs(){
    RotationProbs RP;
    RP.first = { -180, -100, -55, -25, -10, 0, 10, 25, 55, 100, 180 };
    RP.second = {0.0000,0.0000,0.0005,0.0010,0.0050,0.9870,0.0050,0.0010,0.0005,0.0000,0.0000};
    return RP;
}

void Animal::update(sf::Time dt){
    if (counter_since_last_rotation_ > sf::seconds(getAppConfig().animal_next_rotation_delay)){
     //might be trying tu rotate too often
     counter_since_last_rotation_ = sf::Time::Zero; //must only be set back to zero if rotation is of non zero angle
     //rotation of 0 must not be counted
     RotationProbs rot_prob(computeRotationProbs());
     std::piecewise_linear_distribution<> dist(rot_prob.first.begin(), rot_prob.first.end(), rot_prob.second.begin());

     setDirection(getDirection()+(dist(getRandomGenerator())*DEG_TO_RAD));
    }
    counter_since_last_rotation_+=dt;

    move(dt);
    --life_expextancy_;
}


