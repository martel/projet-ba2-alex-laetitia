/*
 * POOSV 2020-21
 * @author: Alexandre Martel et Laetitia Schwitter
 */

#pragma once
#include "Utility/Vec2d.hpp"

/*!
 * @brief Handle toric coordinate, distance and other
 * basic math operation in a toric world
 *
 * @note Gets the dimensions of the world from getAppConfig()
 */

class ToricPosition
{  
private:
    Vec2d coord_; //coordonnées dans le monde
    Vec2d dim_monde_; //dimensions du monde torique dans lequel on se trouve
    void clamp();
public:
    //CONSTRUCTEURS
//    ToricPosition(); // constructeur par défault --> plus efficace dans ToricPosition qui appelle des doubles
    ToricPosition(const Vec2d& vector_coor, const Vec2d& vector_dim);
    ToricPosition(const double& x = 0.0,const double& y = 0.0);
    ToricPosition(const Vec2d& vecteur);
    ToricPosition(const ToricPosition& a_copier) = default; //constructeur de copie already exists for each class by default, hence no need to redefine it.


    //GETTEURS
    Vec2d const& toVec2d() const;
    double x() const; //the tests require getters with the name x() and y() to verify the values of the coord).
    double y() const; // the const is essential for the *this pointer to work, as it requires a const value.

    //OPERATEURS
    ToricPosition& operator=(const ToricPosition&) = default; //interne car on aura besoin d'acceder aux parties privés de la classe, mais existe déjà par défault.
    bool operator==(ToricPosition ) const; //la methode est déclaré const, car l'objet courant n'est pas changé
    ToricPosition& operator+=(ToricPosition const&);
    double operator[](int index) const;

    //METHODES AUTRES
    Vec2d toricVector(ToricPosition const& that) const;

};

//FONCTIONS:

std::ostream& operator<<(std::ostream&, ToricPosition const&);
ToricPosition operator+(ToricPosition, ToricPosition const&); //pas de const car l'objet courant est modifé:)
double toricDistance(ToricPosition const& from, ToricPosition const& to);


