﻿/*
 * POOSV 2020-21
 * @author: Alexandre Martel et Laetitia Schwitter
 */

#include "ToricPosition.hpp"
#include "../Application.hpp"
#include <cmath>
#include "Utility/Utility.hpp"
#include <utility> //--> because of swap.
#include <iostream>
#include <vector>



void ToricPosition::clamp()
{

    double result_modulo_x(fmod(coord_.x(),dim_monde_.x()));
    double result_modulo_y(fmod(coord_.y(),dim_monde_.y()));
    bool mod_x(result_modulo_x< 0); // if mod_x / y are true the constructor will need to create a new Vec2d
    bool mod_y(result_modulo_y< 0);
      //LES DETAILS DU CODE:
//    if (mod_x && mod_y){
//        coord = Vec2d(result_modulo_x + dim_monde.x(), result_modulo_y + dim_monde.y());
//    }
//    else if(mod_x){
//        coord = Vec2d(result_modulo_x + dim_monde.x(), result_modulo_y);
//    }
//    else if(mod_y){
//        coord = Vec2d(result_modulo_x, result_modulo_y + dim_monde.y() );
//    }
//    else {
//        coord = Vec2d(result_modulo_x, result_modulo_y);
//    }
// --> this is very wordy (but works also), hence using i & j as intermediaries seemed more appropriate!
    size_t i(0), j(0);
    if (mod_x){
        i = 1;
    }
    if (mod_y){
        j = 1;
    }
    coord_ = Vec2d(result_modulo_x +i*dim_monde_.x(), result_modulo_y + j*dim_monde_.y());
}

ToricPosition::ToricPosition(const Vec2d& vector_coor, const Vec2d& vector_dim)
    : coord_(vector_coor), dim_monde_(vector_dim)
{
   clamp();
}
ToricPosition::ToricPosition(const double& x,const double& y) //  !  ToricPosition::ToricPosition(const double& x = 0.0,const double& y =0.0) --> this should allow the removal of the "ToricPosition::ToricPosition()"
    : ToricPosition(Vec2d(x,y), Vec2d(getAppConfig().simulation_size, getAppConfig().simulation_size))
{   //no code repetition by calling on other constructor -> only possible cause C++11.
    //no need to call clamp(), since (0.0,0.0) has to be inside of the simulation
}
ToricPosition::ToricPosition(const Vec2d& vecteur)
    : ToricPosition(vecteur.x(), vecteur.y())
{
}

Vec2d const& ToricPosition::toVec2d() const
{
    return coord_;
}
double ToricPosition::x() const{
    return coord_.x();
}
double ToricPosition::y() const{
    return coord_.y();
}

bool ToricPosition::operator==(ToricPosition a) const{
    return (isEqual(x(), a.x()) && isEqual(y(), a.y()));
}
ToricPosition& ToricPosition::operator+=(ToricPosition const& pos2)
{
    coord_ += pos2.coord_;
    clamp();
    return *this;
}
ToricPosition operator+(ToricPosition pos1, ToricPosition const& pos2)
{
   return pos1.operator+=(pos2); //external, as we use operator+= (internal) hence no need to break encapsulation.
}
double ToricPosition::operator[](int index) const
{
    return coord_.operator[](index); //operator[] already exists in Vec2d.

}
Vec2d ToricPosition::toricVector(ToricPosition const& that) const //ce const est parceque l'objet courant reste inchangé et après dans ToricDistance on met en paramÈtre const&.
{
    Vec2d min(that.coord_ - coord_);

    for (int i(-1); i<2; ++i){
        for (int j(-1); j<2; ++j){

            if (distance(that.coord_ + Vec2d(i*dim_monde_.x(), j*dim_monde_.y()), coord_) < min.length()){
                min = that.coord_ + Vec2d(i*dim_monde_.x(), j*dim_monde_.y()) - coord_;
            }
        }
    }
    return min;
    //IDEA: Compare all resulting vectors and store only when the distance is smaller. length() is part of class Vec2d.

}

double toricDistance(ToricPosition const& from, ToricPosition const& to)
{
    return (from.toricVector(to)).length();
}

std::ostream& operator<<(std::ostream& out, ToricPosition const& a)
{
       out << '[' << a.x() << ", " << a.y() << ']';
       return out; //choix: externe car on utilise que les methodes publiques de la classe, donc surcharge externe est favorisé!
}

