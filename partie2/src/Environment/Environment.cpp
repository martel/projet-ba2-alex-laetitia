/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "Environment.hpp"
#include "Animal.hpp"
#include "Food.hpp"
#include "FoodGenerator.hpp"
#include "Application.hpp" //im not sure this goes here or in cpp.
//--> Application directory might be "src/Application.hpp" but it didnt seem to find this!
#include "Random/RandomGenerator.hpp"


//METHODS OF CLASS ENVIRONMENT
void Environment::addAnimal(Animal* animal_being_added){//same for addfood, reference and use of new to control memory allocation outside range instead of pointer which might get corrupted once outside the range of addsmth
    faune_.push_back(animal_being_added);
}
void Environment::addFood(Food* food_being_added){
    food_source_.push_back(food_being_added);
}
void Environment::update(sf::Time dt){
    food_generated_.update(dt);
    //std::cout << "Taille vecteur " << food_source_.size() << std::endl;
        for (auto &animal: faune_){
        if (not animal->isDead()){// no need for the content of the pointer address bc reference?
            animal->update(dt);
        }
        else {
            animal = nullptr;
        }
    }
    faune_.erase(std::remove(faune_.begin(), faune_.end(), nullptr), faune_.end());
    ////TO BE CODED!!!!
}
void Environment::drawOn(sf::RenderTarget& targetWindow){
    for (auto& animal: faune_) animal->Animal::drawOn(targetWindow);
    for (auto& food: food_source_) food->Food::drawOn(targetWindow);
}
void Environment::reset(){
    for (auto& animal: faune_){
        delete animal;
        animal = nullptr;
    }
    for (auto& food: food_source_){
        delete food;
        food = nullptr;
    }
    faune_.clear();
    food_source_.clear();
    //Is there a more efficient way that duplicates less code?
}

Environment::~Environment(){
    reset();
}
