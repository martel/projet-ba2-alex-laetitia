/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once

#include "FoodGenerator.hpp"

//------

#include <vector>
#include <SFML/Graphics.hpp>


class Food;
class Animal;
class FoodGenerator;

typedef std::vector<Animal*> lesAnimaux; //typedef pour augmenter la lisibilité
typedef std::vector<Food*> laNourriture;

class Environment{
//--> je n'ai pas encore décidé si je veux faire private/ protected
    //Attributs
    lesAnimaux faune_; // DEF FAUNE: désigne l'ensemble des espèces animales présentes dans un espace géographique ou un écosystème déterminé
    laNourriture food_source_;
    FoodGenerator food_generated_;
public:
    //Methodes
    void addAnimal(Animal*); //void (et pas const, car on va modifier l'objet courant)
    void addFood(Food*);
    void update(sf::Time dt); ////faire évoluer les animaux de la faune ici:
    void drawOn(sf::RenderTarget& targetWindow); ////faire dessiner les animaux/ nourriture de la faune ici: !!!void might not be correct!
    void reset(); //DEF deletes all food and animals of an ecosystem.
    //constructeur/Destructeurs

    Environment(const Environment&) = delete; //--> forbid copy!
    Environment& operator=(Environment const&) = delete;
    Environment() = default; //Was added ?!? but pointers dont need initialization?!

    //Q2.3 ????
    //--> Idea?! Destructeur that also destroys memory allocation of Animal and Food alongside the memory used by the environment!
    //Alluation dynamique "ptr= new type;" et liberation de la memoire "delete ptr; ptr = nullptr;"
    //--> simultaneously -1 Animal in Faune.
    //Question: How to do that?!
    ~Environment(); //Fait appel à reset().



};
