#include "Anthill.hpp"
#include "Utility/Utility.hpp"
#include "AntSoldier.hpp"
#include "AntWorker.hpp"
#include "Random/Random.hpp"

Uid Anthill::getAnthillId(){
    return identifiant_;
}

Anthill::Anthill(const ToricPosition& pos, const Quantity& initial_stock_food):
    Positionable(pos), stock_nourriture_(initial_stock_food), compteur_new_fourmie_(sf::seconds(getAppConfig().anthill_spawn_delay)),
    identifiant_(createUid())

{
}
void Anthill::drawOn(sf::RenderTarget& target) const
{
    auto const Anthill_sprite = buildSprite((getPosition().toVec2d()),/*size: where do i find that --> 20.0 is completely arbitrary? */ 50.0 ,getAppTexture(getAppConfig().anthill_texture));
    target.draw(Anthill_sprite);

    if (isDebugOn()){
        target.draw(buildText(to_nice_string(identifiant_), getPosition().toVec2d(), getAppFont(), 20, sf::Color::Black));
        target.draw(buildText(to_nice_string(stock_nourriture_), (getPosition() + ToricPosition(0,20) /*this toricposition is just so that both numbers arent on top of eachother */).toVec2d(), getAppFont(), 20, sf::Color::Magenta));
    }
}

void Anthill::update(sf::Time dt)
{
    if (compteur_new_fourmie_> sf::seconds(getAppConfig().anthill_spawn_delay)){
        compteur_new_fourmie_= sf::Time::Zero;
        // Access à l'environment:
        double random_prob(uniform(0,1));

        if((random_prob >= 0.0)&& (random_prob <= getAppConfig().anthill_worker_prob_default)){
            getAppEnv().addAnimal(new AntWorker(getPosition().toVec2d(), identifiant_));
        }
        else getAppEnv().addAnimal(new AntSoldier(getPosition().toVec2d(), identifiant_));
    }
    compteur_new_fourmie_ += dt;

}

void Anthill::setNourriture(const Quantity& amount)
{
    stock_nourriture_ += amount;

}
