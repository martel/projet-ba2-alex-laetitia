/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include <SFML/Graphics.hpp>
#include "Interface/Updatable.hpp"


class FoodGenerator : private Updatable {
private:
    //attributs

    sf::Time compteur_temps_; //<- doesnt this need to be used at some point?XD

    //methodes
public:
    void update(sf::Time dt) override; //changes the time in the compteur_temps, void car on change l'attribut directement

    //constructeur
    FoodGenerator();
};
