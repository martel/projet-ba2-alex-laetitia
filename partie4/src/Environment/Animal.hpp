/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include <utility>
#include "Positionable.hpp"
#include "Utility/Types.hpp"
#include "Animal.hpp"
#include "Application.hpp"
// no need to do #include "Environment.hpp" as application.hpp already contains it.
enum Etat{Attack, Idle , Escaping};
class Termite;
class Ant;
class AntWorker;
class Animal: public Positionable, protected Updatable, protected Drawable {

public:
    Animal(const ToricPosition& pos, int pt_vie = 1, int esp_vie = (getAppConfig().animal_default_lifespan));
    virtual double getSpeed() const = 0;
    Angle getDirection() const ;
    virtual sf::Texture& getTexture() const = 0;
    virtual int getAnimal_Type_strength() const = 0;
    virtual double getAnimal_Type_Attack_delay() const = 0;
    bool isDead() const;
    virtual void drawOn(sf::RenderTarget& target) const override;
    virtual RotationProbs computeRotationProbs() const;
    void update(sf::Time dt) override; //used to udate counter & comput rotation
    virtual bool isEnemy(Animal const* entity) const = 0;
    virtual bool isEnemyDispatch(Termite const* other) const = 0;
    virtual bool isEnemyDispatch(Ant const* other) const = 0;
    int getPt_vie() const;
    void setPt_vie(const int& new_pt_vie);
    Etat getEtat_animal() const;



protected:
    virtual void move(sf::Time const dt);
    void setDirection(const Angle&);
    void setEtat_animal(const Etat& nouveau_etat);
    void setIsAntWorker();

    /*virtual */void inAttack(Animal* opponent, sf::Time dt);
//    virtual void inAttack(AntWorker* opponent_is_an_antworker, sf::Time dt);

    /*!
     * @brief if the Animals are enemies and close enough, their Etat_animal will become Attack.
     * Uses isEnemy and combat (virtual).
     */
    virtual void impact_of_combat_on_opponent(Animal* opponent);
    /*!
     * @brief two Animals will combat eachother by default they will both lose points from their lifespan according to their species.
     */




private:
    Angle direction_;
    int pt_de_vie_;
    int life_expectancy_; //pt_de_vie & life_expectancy are private in part2.
    Etat etat_animal_;
    sf::Time counter_since_last_rotation_;
    sf::Time counter_fight_;
    bool isAntworker_;
};
