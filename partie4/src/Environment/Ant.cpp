#include "Ant.hpp"
#include "Environment.hpp"
#include "Pheromone.hpp"
#include "Utility/Types.hpp"
#include "Utility/Utility.hpp"
#include <cmath>


//comment to force change


Ant::Ant(const ToricPosition& position, const Uid& fourmiliere, int h_p, int life_exp):
    Animal(position, h_p, life_exp), fourmiliere_(fourmiliere), /*Second_to_last_position_(position),*/ Last_Pheromone_position_(position)
{}

Uid Ant::getAnthillId_fourmie() const{
    return fourmiliere_;
}

double Ant::getSpeed() const {
     return getAppConfig().ant_speed;
}

void Ant::spreadPheromone()
{
    Vec2d dX(getLast_Pheromone_position().toricVector(getPosition()));
    double lX(toricDistance(getPosition(),getLast_Pheromone_position()));
   double instances_a_creer(lX * getAppConfig().ant_pheromone_density);

    Vec2d interval_vector_between_both_positions(dX/instances_a_creer);
//    double interv_length(interval_vector_between_both_positions.length()); -> for debug
//double n (instances_a_creer);
//std::cerr<<instances_a_creer;
    for (size_t i(0); i < instances_a_creer ; ++i){
        getAppEnv().addPheromone(new Pheromone(ToricPosition(getLast_Pheromone_position() +i*interval_vector_between_both_positions), getAppConfig().ant_pheromone_energy));

//        setPosition(ToricPosition(getSecond_to_last_position() +i* interval_vector_between_both_positions));
//        std::cerr << "i = " << i << std::endl; --> for debug
       Last_Pheromone_position_ = (getLast_Pheromone_position() +i*interval_vector_between_both_positions);
    }

} //let us try a more conventional iteration


void Ant::move(sf::Time const dt)
{
    Animal::move(dt);
    spreadPheromone();
}

void Ant::drawOn(sf::RenderTarget& target) const
{
    Animal::drawOn(target);
    if (getAppConfig().getProbaDebug())
    {
         display_rot_probs(target);
    }
    if (isDebugOn())
    {
        auto const cercle_olfactif = buildAnnulus(getPosition().toVec2d(), getAppConfig().ant_smell_max_distance, sf::Color::Blue, 5);
            target.draw(cercle_olfactif);
        auto fourmiliereID = buildText(to_nice_string(fourmiliere_), (getPosition().toVec2d() + Vec2d(0, -25)), getAppFont(), 20, sf::Color::Cyan);
            target.draw(fourmiliereID);
    }
;

}

RotationProbs Ant::computeRotationProbs() const {

    RotationProbs RP(Animal::computeRotationProbs());
    Intervals I(RP.first);
    Probs Q(getAppEnv().getPheromoneQuantitiesPerIntervalForAnt(getPosition(), getDirection(), RP.first));
    Probs Pm({0.0000,0.0000,0.0005,0.0010,0.0050,0.9870,0.0050,0.0010,0.0005,0.0000,0.0000});
    Probs Pphi(I.size(), 0);
    Probs Pm_prime(I.size(), 0);
    double beta(getAppConfig().beta_d);
    double Q_zero(getAppConfig().q_zero);
    double sum_of_Pphi_elements(0);
    double z_of_alpha(0);
    double alpha(getAppConfig().alpha);
    for (size_t i(0); i < I.size(); ++i)
    {
        Pphi[i] = 1/(1+exp(-beta*(Q[i] - Q_zero)));
        sum_of_Pphi_elements += Pphi[i];
    }

    for (auto& pphi: Pphi)
    {
        pphi /=  sum_of_Pphi_elements;
    }

    for (size_t i(0); i < I.size(); ++i)
    {
        Pm_prime[i] = Pm[i] * pow(Pphi[i], alpha);
        z_of_alpha += Pm_prime[i];
    }

    for (auto& pm_prime: Pm_prime)
    {
        pm_prime /= z_of_alpha;
    }

    RP.second = Pm_prime;//the use of several loops in required as values have to be normalised before we can use them
    //to compute using getPheromoneQuantitiesPerIntervalForAnt
    return RP;

}

ToricPosition Ant::getLast_Pheromone_position(){
    return Last_Pheromone_position_;
}

void Ant::display_rot_probs(sf::RenderTarget& target, Quantities intervals) const //we need a target & intervals as args
{
    auto const intervalProbs = computeRotationProbs();
       // pour intervalProbs (first désigne l'ensemble des angles)
       for (std::size_t i = 0; i < intervalProbs.first.size(); ++i) {
           // "second" designe l'ensemble des probabilités
           auto const msg = std::to_string(intervalProbs.second[i]).substr(2, 4);
           auto const angle = intervalProbs.first[i];
           auto const local = Vec2d::fromAngle(getDirection() + angle * DEG_TO_RAD) * 250;

           auto const text = buildText(msg, getPosition().toVec2d() + local, getAppFont(), 15, sf::Color::Black);
           target.draw(text);
       }

       auto const quantities = getAppEnv().getPheromoneQuantitiesPerIntervalForAnt(getPosition(), getDirection(), intervals);
       for (std::size_t i = 0; i < quantities.size(); ++i) {
           auto const msg = std::to_string(quantities[i]).substr(0, 4);
           auto const angle = intervals[i];
           auto const local = Vec2d::fromAngle(getDirection() + angle * DEG_TO_RAD) * 200;

           auto const text = buildText(msg, getPosition().toVec2d() + local, getAppFont(), 15, sf::Color::Red);
           target.draw(text);
       }
}

bool Ant::isEnemy(Animal const* animal) const
{
    return !isDead() && !animal->isDead() && animal->isEnemyDispatch(this);
}

bool Ant::isEnemyDispatch(Termite const* autre_termite) const {
    return true;
}
bool Ant::isEnemyDispatch(Ant const* autre_fourmi) const
{
   return !(this->getAnthillId_fourmie() == autre_fourmi->getAnthillId_fourmie());
}
double Ant::getAnimal_Type_Attack_delay() const
{
   return getAppConfig().ant_attack_delay;
}


