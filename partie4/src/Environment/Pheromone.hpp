#ifndef PHEROMON_HPP
#define PHEROMON_HPP
#pragma once
#include "Environment.hpp"
#include "Positionable.hpp"

class Pheromone: public Positionable, public Drawable, public Updatable{
private:
    Quantity Quant_pheromone_;
    sf::Color couleur_pheromone_;
protected:
public:
    Pheromone(const ToricPosition& pos, const Quantity&);
    void drawOn(sf::RenderTarget& target) const override;
    void update(sf::Time dt) override;
    bool isNegligible();
    Quantity getQuantity() const;
};

#endif // PHEROMON_HPP
