#ifndef ANTSOLDIER_HPP
#define ANTSOLDIER_HPP
#pragma once

#include "Ant.hpp"

class AntSoldier: public Ant{

public:
    AntSoldier(const ToricPosition& position, const Uid& ant_hill);
    sf::Texture &getTexture() const override;
    int getAnimal_Type_strength() const override;
    void drawOn(sf::RenderTarget& target) const override;
};

#endif // ANTSOLDIER_HPP
