/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "Food.hpp"
#include "Utility/Utility.hpp"
#include "Application.hpp"
#include "ToricPosition.hpp"

//internal:
Food::Food(const ToricPosition& pos, const Quantity& quant)
    : Positionable(pos), quantite_disponible_(quant){
}
Quantity Food::takeQuantity(Quantity quantity_want_taken) {
   if (quantity_want_taken > quantite_disponible_){//can also be equal no?
       quantity_want_taken=quantite_disponible_;
   }
   quantite_disponible_-=quantity_want_taken;
   return quantity_want_taken;
}

void Food::drawOn(sf::RenderTarget& target) const{
    Vec2d position_de_Food(getPosition().toVec2d());
    double une_taille_graphique(quantite_disponible_/2.0);
    std::string texture_de_food(getAppConfig().food_texture);

    auto const foodSprite = buildSprite(position_de_Food, une_taille_graphique, getAppTexture(texture_de_food));
    target.draw(foodSprite);
    //affichage en mode "debugging"
    if (isDebugOn()){
        //parametres de buildText
        //std::string const& msg, Vec2d const& position, sf::Font const& font, unsigned int size,
        //sf::Color color, float rotation = 0.f
        auto const text = buildText(to_nice_string(quantite_disponible_) /* string msg qui s'affiche pour comprendre ce qui se passe*/, position_de_Food, getAppFont(), 15, sf::Color::Black);
        target.draw(text);
    }
}
