#ifndef STATS_HPP
#define STATS_HPP

#include <memory>
#include <vector>
#include "Interface/Drawable.hpp"
#include "Interface/Updatable.hpp"
#include "Utility/Vec2d.hpp"
#include <unordered_map>
#include "Graph.hpp"


class Graph;

//typedef std::vector<std::unique_ptr<Graph>> Graphs;
//typedef std::vector<std::string> Labels;


class Stats : protected Updatable, protected Drawable{
public:
    void setActive(int id);
//    int getActive_id() const;
    std::string getCurrentTitle() const;
    void next();
    void previous();
    void drawOn(sf::RenderTarget& target) const override;
    void reset();
    void addGraph(
      int id,
      const std::string &title,
      const std::vector<std::string> &series,
      double min,
      double max,
      const Vec2d &size
    );
    void update(sf::Time dt) override;
    Stats();


private:

//    Graphs graphs_;//wondering if I should use a map so addgraph is not lost if the size is zero
//    Labels labels_;
    std::unordered_map<int ,std::unique_ptr<Graph>> graphs_;
    std::unordered_map<int ,std::string> labels_;
    int active_id_;
    sf::Time counter_since_last_update_;

};

//I think the compiling problem is that zero length dynamic array (vector) are seen as incomplete type and
//therefore sizeof can't be used

//finalapp uses application::addGraph which itself uses Stats::addGraph

#endif // STATS_HPP
