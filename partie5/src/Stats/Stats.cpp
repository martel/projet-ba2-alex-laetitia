//#include "Stats/Graph.hpp"

#include "Graph.hpp"
#include "Stats.hpp"
#include "Application.hpp"
#include <memory>
#include <vector>

void Stats::setActive(int id){
    active_id_ = id;
}

//int Stats::getActive_id() const{
//    return active_id_;
//}

std::string Stats::getCurrentTitle() const{
    return labels_.find(active_id_)->second;
}

void Stats::next(){
    ++active_id_;
    if (active_id_ > (labels_.size() - 1)){ active_id_ = 0;
    std::cerr << "coucou" << std::endl;
    }
    std::cerr << "next was called : " << active_id_ << std::endl;
    std::cerr << "label size" << labels_.size() << std::endl;
}

void Stats::previous(){
    --active_id_;
    if (active_id_ < 0) {active_id_ = (labels_.size() -1);}
    std::cerr << "previous was called : " << active_id_ << std::endl;
}

void Stats::drawOn(sf::RenderTarget& target) const{
//    for (auto& it : labels_){
////        if (it.first == active_id_) graphs_.find(it.first)->second->drawOn(target);
////        if(it.first == active_id_){
////            graphs_.at(it.first)->Graph::drawOn(target);
////        }

//    }
    graphs_.find(active_id_)->second->drawOn(target);

}

void Stats::reset(){
    for (auto& it : graphs_){
        it.second->reset();
    }
}

void Stats::addGraph( //what if I use a map?
  int id,
  const std::string &title,
  const std::vector<std::string> &series,
  double min,
  double max,
  const Vec2d &size
        ){
    //std::unordered_map<int, std::unique_ptr<Graph>>::iterator it;
//    for (auto& g : graphs_)
//    {
//        if (g.first == id) g.second.reset(new Graph(series, size, min, max)); labels_[id] = title;
//    }
//    graphs_.[id].
//    labels_.at(id) = title;
    graphs_[id].reset(new Graph(series, size, min, max)); labels_[id] = title;
    active_id_ = id;
    std::cerr<<labels_[id]<<std::endl;

}

void Stats::update(sf::Time dt){
    if (counter_since_last_update_ > sf::seconds(getAppConfig().stats_refresh_rate)) //not how we were told to code it but it works
    {

        for (auto& graph : graphs_)
        {

            graph.second->updateData(counter_since_last_update_, getAppEnv().fetchData(labels_[graph.first]));

        }
            counter_since_last_update_ = sf::Time::Zero;
            std::cerr << "graphs size" << graphs_.size() << std::endl;

    }
    counter_since_last_update_ += dt;
}

Stats::Stats():
    graphs_(), labels_(), active_id_(0), counter_since_last_update_(sf::Time::Zero)

{}
