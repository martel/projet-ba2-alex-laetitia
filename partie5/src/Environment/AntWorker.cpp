#include "AntWorker.hpp"
#include "Anthill.hpp"
#include "Food.hpp"
#include "Utility/Utility.hpp"
#include <cmath>
#include "Application.hpp"

int AntWorker::antworker_counter_(0);

AntWorker::AntWorker(const ToricPosition& position, const Uid& ant_hill) :
    Ant(position, ant_hill, getAppConfig().ant_worker_hp, getAppConfig().ant_worker_lifespan)
{
    setIsAntWorker();
    ++antworker_counter_;
}

sf::Texture& AntWorker::getTexture() const
{
    return getAppTexture(getAppConfig().ant_worker_texture);
}
int AntWorker::getAnimal_Type_strength() const
{
    return getAppConfig().ant_worker_strength;
}
void AntWorker::drawOn(sf::RenderTarget& target) const
{
        //Drawing ANTWORKER
//        auto const Worker_sprite = buildSprite((getPosition().toVec2d()),/*size: where do i find that --> 20.0 is completely arbitrary? */ 50.0 ,getAppTexture(getAppConfig().ant_worker_texture), getDirection()/DEG_TO_RAD);
//        target.draw(Worker_sprite);
        Ant::drawOn(target);
        if (isDebugOn()){
            auto PorteFood_text = buildText(to_nice_string(food_carrying_), (getPosition().toVec2d() + Vec2d(0, 25)), getAppFont(), 20, sf::Color::Magenta);
            target.draw(PorteFood_text);
        }
}
void AntWorker::update(sf::Time dt)
{
    Animal::update(dt);
    if ((food_carrying_ == 0.0) && getAppEnv().getClosestFoodForAnt(getPosition()) != nullptr){

        food_carrying_= (getAppEnv().getClosestFoodForAnt(getPosition())->takeQuantity(getAppConfig().ant_max_food));
        setDirection(getDirection() + PI);
    }
    if ((food_carrying_ != 0.0) && (getAppEnv().getAnthillForAnt(getPosition(), getAnthillId_fourmie()) != nullptr)){
        (getAppEnv().getAnthillForAnt(getPosition(), getAnthillId_fourmie()))->setNourriture(food_carrying_);
        food_carrying_ =0.0;
    }
}

int AntWorker::getAntWorkerCounter(){
    return antworker_counter_;
}

AntWorker::~AntWorker(){--antworker_counter_;}




