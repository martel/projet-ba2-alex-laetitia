#ifndef TERMITE_HPP
#define TERMITE_HPP
#pragma once
#include "Animal.hpp"

//Add Termite to Env (incomplete class)
class Termite: public Animal{
public:
//    Termite(const ToricPosition& Pos, const int& point_vie = getAppConfig().termite_hp, const int& duree_vie = getAppConfig().termite_lifespan);
    Termite(const ToricPosition& Pos, const int& point_vie, const int& duree_vie);
    Termite(const ToricPosition& Pos);
    sf::Texture& getTexture() const override;
    int getAnimal_Type_strength() const override;
    double getAnimal_Type_Attack_delay() const override;
    double getSpeed() const override;
    bool isEnemy(Animal const* animal) const override;
    bool isEnemyDispatch(Termite const* autre_Termite) const override;
    bool isEnemyDispatch(Ant const* autre_fourmi) const override;
    ~Termite();
    static int getTermitecounter();
protected:
private:
    static int termite_counter_;

};


//A COPIER DANS animal.hpp:


//A COPIER DANS animal.cpp:

//A COPIER DANS ant.hpp:


//A COPIER DANS ant.cpp:

//A COPIER DANS antworker.hpp:
//A COPIER DANS antworker.cpp:

#endif // TERMITE_HPP
