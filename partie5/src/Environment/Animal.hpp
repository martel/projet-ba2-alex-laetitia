/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include <utility>
#include "Positionable.hpp"
#include "Utility/Types.hpp"
#include "Animal.hpp"
#include "Environment.hpp"

enum Etat{Attack, Idle , Escaping};
class Termite;
class Ant;
class AntWorker;
class Animal: public Positionable, protected Updatable, protected Drawable {

public:

    /*!
     * @brief create animal with direct control over the start value of some of its attributes
     *
     * @param position
     * @param health points
     * @param life expectancy
     */
    Animal(const ToricPosition& pos, int pt_vie , int esp_vie);
    /*!
     * @brief create animal at achosen position
     */
    Animal(const ToricPosition& pos);
    virtual double getSpeed() const = 0;
    Angle getDirection() const ;
    virtual sf::Texture& getTexture() const = 0;
    virtual int getAnimal_Type_strength() const = 0;
    virtual double getAnimal_Type_Attack_delay() const = 0;
    /*!
     * @brief know if we need to delete the animal
     */
    bool isDead() const;
    /*!
     * @brief draw the visual rendition of the simulated animal
     * @param uses the graphical interface
     */
    virtual void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief know how likely an animal is to turn in a particular direction
     */
    virtual RotationProbs computeRotationProbs() const;
    /*!
     * @brief call move and compute rotation probs
     */
    void update(sf::Time dt) override; //used to udate counter & comput rotation
    /*!
     * @brief know if a given animal is an ennemy of current object
     */
    virtual bool isEnemy(Animal const* entity) const = 0;
    /*!
     * @brief used by isEnemey
     */
    virtual bool isEnemyDispatch(Termite const* other) const = 0;
    /*!
     * @brief used by isEnnemy
     */
    virtual bool isEnemyDispatch(Ant const* other) const = 0;
    int getPt_vie() const;
    /*!
     * @brief modify the health of an animal, eg. to reduce it after/during a fight
     */
    void setPt_vie(const int& new_pt_vie);
    Etat getEtat_animal() const;
    //animal_type getAnimalType() const;
    virtual ~Animal();



protected:
    /*!
     * @brief change position linearly
     *
     * @param delta de temps
     *
     */
    virtual void move(sf::Time const dt);
    /*!
     * @brief set direction to chosen value
     *
     * @param angle in rad
     *
     */
    void setDirection(const Angle&);
    /*!
     * @brief change animal state between idle, attacking and fleeing
     *
     * @param new state
     *
     */
    void setEtat_animal(const Etat& nouveau_etat);
    /*!
     * @brief know is the ant will react when faced with another animal
     */
    void setIsAntWorker();
    /*!
     * @brief change the state of the animal appropriately according to animal type
     */
    void inAttack(Animal* opponent, sf::Time dt);
    /*!
     * @brief if the Animals are enemies and close enough, their Etat_animal will become Attack.
     * Uses isEnemy and combat (virtual).
     */
    virtual void impact_of_combat_on_opponent(Animal* opponent);
    /*!
     * @brief two Animals will combat eachother by default they will both lose points from their lifespan according to their species.
     */




private:
    Angle direction_;
    int pt_de_vie_;
    int life_expectancy_; //pt_de_vie & life_expectancy are private in part2.
    Etat etat_animal_;
    sf::Time counter_since_last_rotation_;
    sf::Time counter_fight_;
    bool isAntworker_;
    //animal_type is_a_;
};
