/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "Environment.hpp"
#include "Animal.hpp"
#include "Food.hpp"
#include "Anthill.hpp"
#include "Pheromone.hpp"
#include "FoodGenerator.hpp"
#include "Application.hpp" //im not sure this goes here or in cpp.
//--> Application directory might be "src/Application.hpp" but it didnt seem to find this!
#include "Random/RandomGenerator.hpp"
#include <cmath>
#include "Utility/Utility.hpp"
#include "Config.hpp"
#include <unordered_map>
#include <string>
#include "AntSoldier.hpp"
#include "AntWorker.hpp"
#include "Termite.hpp"

typedef std::vector<Animal*> lesAnimaux; //typedef pour lisibilité

//METHODS OF CLASS ENVIRONMENT
void Environment::addAnimal(Animal* animal_being_added){//same for addfood, reference and use of new to control memory allocation outside range instead of pointer which might get corrupted once outside the range of addsmth
    faune_.push_back(animal_being_added);
}
void Environment::addFood(Food* food_being_added){
    food_source_.push_back(food_being_added);
}
void Environment::addAnthill(Anthill *anthill_being_added){
    anthills_.push_back(anthill_being_added);
}
void Environment::addPheromone(Pheromone *pheromone_being_added){
//    std::cerr <<"pheromone was added" << std::endl; --> for debug
    pheromones_.push_back(pheromone_being_added);
}
Food* Environment::getClosestFoodForAnt(ToricPosition const& position_fourmie) const
{
//    size_t index(0);
//    if (not food_source_.empty()){

//        double dist_min(toricDistance(position_fourmie,(food_source_[0])->getPosition()));

//        for (size_t i(1); i < food_source_.size(); ++i){
//                if (toricDistance(position_fourmie,(food_source_[i]->getPosition())) < dist_min){
//                dist_min = toricDistance(position_fourmie,(food_source_[i]->getPosition()));
//                index = i;

//            }

//        }
//        if (dist_min < getAppConfig().ant_max_perception_distance){
//            return food_source_[index];
//        }
//        else return nullptr;

//    }
//    else return nullptr;
    if (food_source_.empty()){
        return nullptr;
    }
    else {
        if (closestFromPoint(position_fourmie, food_source_) != nullptr && toricDistance(closestFromPoint(position_fourmie, food_source_)->getPosition(), position_fourmie) <= getAppConfig().ant_max_perception_distance)
        {
            return closestFromPoint(position_fourmie, food_source_);
        }
        else
        {
            return nullptr;
        }
    }

}
Anthill* Environment::getAnthillForAnt(ToricPosition const& position, Uid anthillId) const
{
//    if (not anthills_.empty()){
//        for (size_t i(0); i< anthills_.size(); ++i){
//            if ((anthills_[i]->getAnthillId() == anthillId)
//                    && (toricDistance(position,anthills_[i]->getPosition()) <= getAppConfig().ant_max_perception_distance)){
//                return anthills_[i];
//                /*!!!! What should it return if the Anthill is not close?! */
//            }
//        /* there should never be an else unless, the ants were not properly distroyed*/
//        }
//        return nullptr;
//    }
//    else return nullptr;
    if (anthills_.empty()){
        return nullptr;
    }
    else {
        if (closestFromPoint(position, anthills_) !=nullptr && ((toricDistance(closestFromPoint(position, anthills_)->getPosition(), position) <= getAppConfig().ant_max_perception_distance))
                && (closestFromPoint(position,anthills_)->getAnthillId() == anthillId)){
            return closestFromPoint(position, anthills_);
        }
        else
        {
            return nullptr;
        }
    }



}
Animal* Environment::getClosestEnemyforAnimal(Animal const* current_animal) const
{
    if (faune_.size() == 1){
        return nullptr;
    }
//    if((closestFromPoint(current_animal->getPosition(),faune_) !=nullptr) && (toricDistance(closestFromPoint(current_animal->getPosition(), faune_)->getPosition(), current_animal->getPosition()) <= getAppConfig().animal_sight_distance)){
//        std::cerr << "im in closest Enemy for Animal"<< std::endl;
//        return closestFromPoint(current_animal->getPosition(),faune_);
//    }
    double min_distance (sqrt(2*pow(getAppConfig().simulation_size,2)));
    Animal* Enemy(nullptr);
    for (auto& animal : faune_)
    {   if (animal == nullptr) continue; //If the animal created first dies during a fight, while we are still iterating on the faun, we might get a nullptr and we'd want to access a nullptr. It crashes.
        if ((toricDistance(animal->getPosition(), current_animal->getPosition()) < min_distance) &&
                (current_animal->isEnemy(animal))) //the current animal is its own friend --> will not affect min_dist to 0.
        {
            min_distance = toricDistance(animal->getPosition(), current_animal->getPosition());
            if (min_distance <= getAppConfig().animal_sight_distance)
            {
                Enemy = animal;
            }
        }
    }
    return Enemy;

}
void Environment::update(sf::Time dt){
    food_generated_.update(dt);
    //std::cout << "Taille vecteur " << food_source_.size() << std::endl;
        for (auto &animal: faune_){
        if (animal->isDead()){// no need for the content of the pointer address bc reference?
            delete animal;
            animal = nullptr; //shouldn't it be delete?!
        }
        else {
            animal->update(dt);
        }
    }
    faune_.erase(std::remove(faune_.begin(), faune_.end(), nullptr), faune_.end());
//    for (auto& animal: faune_){
//        if(!animal->isDead()){
//            animal->update(dt);
//        }
//    }
//    faune_.erase(std::remove_if(faune_.begin(),faune_.end(), [](Animal* x){ return x->isDead();}),faune_.end()); --> works too, is fancier and uses lamda (if i do this, no need to have the line : "if (animal == nullptr) continue;", cause no nullptr is ever created.

    for (auto& anthill: anthills_){
        anthill->update(dt);
    }
    for (auto& pheromone: pheromones_){
        pheromone->update(dt);
    }
//    std::cerr << "nbr pheromones : " << pheromones_.size()<< std::endl; --> for debug
    ////TO BE CODED!!!!
}
void Environment::drawOn(sf::RenderTarget& targetWindow) const{
    for (auto& animal: faune_) animal->drawOn(targetWindow); //ne plus: animal-> Animal::drawOn puisqu'on veut faire appel a la fonction DrawOn du type courant
    for (auto& food: food_source_) food->/*Food::*/drawOn(targetWindow);
    for (auto& anthill: anthills_) anthill->drawOn(targetWindow);
//    if (pheromones_should_be_drawn_){
//    if (pheromones_.empty()) std::cout<<"no pheromones detected"<<std::endl;
        for (auto& pheromone: pheromones_)
        {
            pheromone->drawOn(targetWindow);

        }
//        std::cerr<< "true" <<std::endl;
//    }
//   else {
//        std::cerr<< "false " << std::endl;
//      }
}
void Environment::reset(){
    for (auto& animal: faune_){
        delete animal;
        animal = nullptr;
    }
    for (auto& food: food_source_){
        delete food;
        food = nullptr;
    }
    for (auto& anthill: anthills_){
        delete anthill;
        anthill = nullptr;
    }
    for (auto& pheromone: pheromones_){
        delete pheromone;
        pheromone = nullptr;
    }
    faune_.clear();
    food_source_.clear();
    anthills_.clear();
    pheromones_.clear();
    //Is there a more efficient way that duplicates less code?
}

Environment::~Environment(){
    reset();
}

bool Environment::togglePheromoneDisplay()
{
    pheromones_should_be_drawn_ = (not pheromones_should_be_drawn_);
//    if (pheromones_should_be_drawn_) std::cerr<<"true"<<std::endl;
//    else std::cerr<<"false"<<std::endl;
    return pheromones_should_be_drawn_;
}

bool Environment::getAuthorization_to_draw_pheromones() const
{
    return pheromones_should_be_drawn_;
}

Quantities Environment::getPheromoneQuantitiesPerIntervalForAnt(
  const ToricPosition &position,
  Angle direction_rad,
  const Intervals &angles
        )
{
    Quantities PheromoneQuantitiesPerInterval(angles.size(), 0);
    for (auto& pheromone: pheromones_)
    {
        if ((toricDistance(position, pheromone->getPosition()) < getAppConfig().ant_smell_max_distance) and not pheromone->isNegligible())
        {
            Angle AngleWithAnt_deg_beta(angleDelta((position.toricVector(pheromone->getPosition())).angle(), direction_rad));
            AngleWithAnt_deg_beta /=DEG_TO_RAD;

            if (!angles.empty()){
                Angle closest_to_angle_with_ant_beta_prime(angles[0]);
                int index_of_closest_angle(0);
                for (size_t i(1); i < angles.size(); ++i)
                {
                    if (std::abs(AngleWithAnt_deg_beta - angles[i]) < std::abs(AngleWithAnt_deg_beta - closest_to_angle_with_ant_beta_prime))
                    {
                        closest_to_angle_with_ant_beta_prime = angles[i];
                        index_of_closest_angle=i;
                    }
                }
                PheromoneQuantitiesPerInterval[index_of_closest_angle] += pheromone->getQuantity();
                if (index_of_closest_angle == ((0) or (angles.size() - 1))){
//                    PheromoneQuantitiesPerInterval[0] = PheromoneQuantitiesPerInterval[index_of_closest_angle];
                    PheromoneQuantitiesPerInterval[angles.size()-1] = PheromoneQuantitiesPerInterval[index_of_closest_angle];
                }
            }

        }
    }

    //first iterate on pheromones, if they are within detection range & are not negligible, find
    //the angle, find which angle in the inetervals given ic closest & add amount to vector of
    //quantities that I want to return
return PheromoneQuantitiesPerInterval;
}

std::vector<std::string> Environment::getAnthillsIds() const{
    std::vector<std::string> Ids;
    for (const auto& anthill : anthills_){
        Ids.push_back("anthill #" + std::to_string(anthill->getAnthillId()));
    }
    return Ids;
}

std::unordered_map<std::string, double> Environment::fetchData(const std::string & title) const{//problem with switch, apparently doesn't like strings
    std::unordered_map<std::string, double> result;
    //switch (title) weird that it doen't work
    if(title == s::GENERAL){
    result = {
       {s::WORKER_ANTS, AntWorker::getAntWorkerCounter()},
       {s::SOLDIER_ANTS, AntSoldier::getAntSoldierCounter()},
       {s::TERMITES, Termite::getTermitecounter()},
       {s::TEMPERATURE, getTemperature()}
    };
    }
    else if (title == s::FOOD){
        result = {{s::FOOD, Food::getFoodCounter()}};
    }
    else if(title == s::ANTHILLS) {
        size_t i(0);
        for (auto id : getAnthillsIds())
        {
            std::cerr << "im in anthill thing "<< std::endl;
            result.insert({id, anthills_[i]->getStock_nourriture()});
            ++i;
        }

    }
    else if (title == s::WORKER_ANTS){
        result = {{s::WORKER_ANTS, AntWorker::getAntWorkerCounter()}};
    }
    else if(title == s::SOLDIER_ANTS){
       result = {{s::SOLDIER_ANTS, AntSoldier::getAntSoldierCounter()}};
    }
    else if (title == s::TERMITES){
        result = {{s::TERMITES, Termite::getTermitecounter()}};
    }
    else if (title == s::TEMPERATURE){
        result = {{s::TEMPERATURE, getTemperature()}};
    }

    else{ std::cerr << "SOUCIIIIS "<< std::endl;
        result = {{"hi", 2000.2}};
    }
   return result;

}


////template <typename type>//not sure a template is useful
//double Environment::animal_counter(animal_type a_t_) const
//{
//    int result(0);
//    for (const auto& animal : getAppEnv().getFaune())
//    {
//        if (animal->getAnimalType() == a_t_ and not(animal->isDead())) ++result; //calls getanimaltype
//    }
//    return result;
//}

lesAnimaux Environment::getFaune() const{
    return faune_;
}

double Environment::getTemperature() const
{
    return temperature_;
}

