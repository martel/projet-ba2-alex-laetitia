#include "Termite.hpp"
#include "Application.hpp"

//Termite::Termite(const ToricPosition& Pos, const int& point_vie, const int& duree_vie)
//    : Animal(Pos, point_vie, duree_vie, termite)
//{

//}

int Termite::termite_counter_(0);

Termite::Termite(const ToricPosition& Pos, const int& point_vie, const int& duree_vie)
    : Animal(Pos, point_vie, duree_vie)
{
++termite_counter_;
}
Termite::Termite(const ToricPosition& Pos)
    :Termite(Pos, getAppConfig().termite_hp, getAppConfig().termite_lifespan)
{}
sf::Texture& Termite::getTexture() const{
    return getAppTexture(getAppConfig().termite_texture);
}
int Termite::getAnimal_Type_strength() const
{
    return getAppConfig().termite_strength;
}
double Termite::getAnimal_Type_Attack_delay() const{
    return getAppConfig().termite_attack_delay;
}
double Termite::getSpeed() const{
    return getAppConfig().termite_speed;
}
bool Termite::isEnemy(Animal const* animal) const
{
    return (!isDead() && !animal->isDead() && animal->isEnemyDispatch(this));
}

bool Termite::isEnemyDispatch(Termite const* autre_termite) const {
    return false;
}
bool Termite::isEnemyDispatch(Ant const* autre_fourmi) const
{
    return true;
}

Termite::~Termite(){--termite_counter_;}

int Termite::getTermitecounter(){
    return termite_counter_;
}
