#include "Loader.hpp"
#include "Utility/Constants.hpp"
#include "Anthill.hpp"
#include "Food.hpp"
#include "Termite.hpp"
#include "Utility/Vec2d.hpp"
#include "Utility/Utility.hpp"
#include "Environment.hpp"
#include "Application.hpp"
#include <iostream>
#include <sstream>
#include <fstream> //not necessarily useful
#include <limits>
#include <vector>

void loadMap(std::string const& filepath)
{
    std::ifstream entry;
    entry.open(filepath.c_str());
    if (entry.fail())
    {
        std::cerr << "Erreur : impossible de lire le fichier " << std::endl;
        entry.clear();
        entry.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //to reject the rest of the line.
    }
    else
    {
        std::vector<std::string> lines;
        std::string current_line;

        while (std::getline(entry, current_line))
        {
            lines.push_back(current_line);
        }

        for (int i(0); i<lines.size();++i)
        {
         if (lines[i].empty() or (lines[i][0] == '#'))  continue;
         else /*if (lines[i][0] != '#' and lines[i][0] != '') */{
            std::vector<std::string> line_being_read(split(lines[i], ' '));
            if (line_being_read[0] == s::FOOD)
            {
                getAppEnv().addFood(new Food(ToricPosition(std::stoi(line_being_read[1]), std::stoi(line_being_read[2])),
                        std::stoi(line_being_read[3])));
            }
           else if (line_being_read[0] == "anthill")
            {
                getAppEnv().addAnthill(new Anthill(ToricPosition(std::stoi(line_being_read[1]),std::stoi(line_being_read[2]))));
            }
            else if (line_being_read[0] == "termite")
            {
                getAppEnv().addAnimal(new Termite(ToricPosition(std::stoi(line_being_read[1]),std::stoi(line_being_read[2]))));
            }
        }

    }
     entry.close();
    }
}
