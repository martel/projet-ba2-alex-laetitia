/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

//comment to make sure git works
//#include "Animal.hpp"
//#include <utility> no need, is in .hpp
//#include "Random/RandomGenerator.hpp" don't think we need that
#include <SFML/Graphics.hpp>
#include "Utility/Utility.hpp"
#include "Environment.hpp"
#include "Animal.hpp"
#include "Utility/Constants.hpp"
#include "Random/Random.hpp"
#include "Random/RandomGenerator.hpp"
#include "Ant.hpp"
#include "Termite.hpp"
#include "Application.hpp"


//displaced getSpeed code to Ant class

Angle Animal::getDirection() const {
    return direction_;
}

void Animal::setDirection(const Angle& alpha){
    direction_ = alpha;
}

bool Animal::isDead() const {
    return ((pt_de_vie_<=0) or (life_expectancy_<=0));
}

void Animal::drawOn(sf::RenderTarget& target) const{


//    //need to find a way to rotate the sprite, tried using rotate (supposed to be in sf) but not found and sprite is const so no luck there
//    //reference: drawOn in Food:)
//    //animal_sprite.setRotation(direction_);//does not work bc sprite is const
//    target.draw(animal_sprite);
//    // was moved to --> AntSoldier and AntWorker
    auto const Animal_sprite = buildSprite((getPosition().toVec2d()),/*size: where do i find that --> 20.0 is completely arbitrary? */ 50.0 , getTexture(), getDirection()/DEG_TO_RAD);
    target.draw(Animal_sprite);
    if (isDebugOn()){
        auto const text = buildText(to_nice_string(pt_de_vie_), ((getPosition().toVec2d()) + Vec2d(0, 25)), getAppFont(), 20, sf::Color::Red);
        target.draw(text);
        sf::VertexArray ligne(sf::PrimitiveType::Lines, 2);
        ligne[0]={(getPosition().toVec2d()), sf::Color::Black};//need to find a way to code arrival coord
        ligne[1]={((getPosition().toVec2d())+((Vec2d::fromAngle(getDirection())) * 60.0) /*arbitrary*/), sf::Color::Black};
        target.draw(ligne);
        auto const cercle_enemy_sensor = buildAnnulus(getPosition().toVec2d(), getAppConfig().animal_sight_distance, sf::Color::Red, 5);
            target.draw(cercle_enemy_sensor);
    }
}

//Animal::Animal(const ToricPosition& pos, int pt_vie, int esp_vie, animal_type a_t)
//    : Positionable(pos), direction_(uniform(0.0, TAU)), pt_de_vie_(pt_vie), life_expectancy_(esp_vie),
//      etat_animal_(Idle), counter_since_last_rotation_(sf::Time::Zero), counter_fight_(sf::Time::Zero), isAntworker_(false), is_a_(a_t)
//{}

Animal::Animal(const ToricPosition& pos, int pt_vie, int esp_vie/*, animal_type a_t*/)
    : Positionable(pos), direction_(uniform(0.0, TAU)), pt_de_vie_(pt_vie), life_expectancy_(esp_vie),
      etat_animal_(Idle),  counter_since_last_rotation_(sf::Time::Zero), counter_fight_(sf::Time::Zero), isAntworker_(false)
{}

Animal::Animal(const ToricPosition& pos)
    : Animal(pos, 1, getAppConfig().animal_default_lifespan)
{

}


void Animal::move(sf::Time dt){
    if (etat_animal_ != Attack){
//        std::cerr <<"move" << std::endl;
        auto dx = getSpeed()*(Vec2d::fromAngle(getDirection())) * dt.asSeconds();
        setPosition(getPosition() + ToricPosition(dx));
    }
}
RotationProbs Animal::computeRotationProbs() const{
    RotationProbs RP;
    RP.first = { -180, -100, -55, -25, -10, 0, 10, 25, 55, 100, 180 };
    RP.second = {0.0000,0.0000,0.0005,0.0010,0.0050,0.9870,0.0050,0.0010,0.0005,0.0000,0.0000};
    return RP;
}

void Animal::update(sf::Time dt){
    if (counter_since_last_rotation_ > sf::seconds(getAppConfig().animal_next_rotation_delay)){
     //might be trying tu rotate too often
     counter_since_last_rotation_ = sf::Time::Zero; //must only be set back to zero if rotation is of non zero angle
     //rotation of 0 must not be counted
     RotationProbs rot_prob(this->computeRotationProbs());
     std::piecewise_linear_distribution<> dist(rot_prob.first.begin(), rot_prob.first.end(), rot_prob.second.begin());

     setDirection(getDirection()+(dist(getRandomGenerator())*DEG_TO_RAD));
    }
    counter_since_last_rotation_+=dt;
//    if (getAppEnv().getClosestAvailableEnemyforAnimal(this) == this){
//       std::cerr << "ERRRRRRRROOOOORRRR" << std::endl;
//    }-->for debug
    --life_expectancy_;
    inAttack(getAppEnv().getClosestEnemyforAnimal(this),dt);
    move(dt);
//    std::cerr << "je suis dans update"<< std::endl; /*->for debug*/



}
Etat Animal::getEtat_animal() const
{
    return etat_animal_;
}

void Animal::setEtat_animal(const Etat& nouveau_etat){
    etat_animal_ = nouveau_etat;
}

void Animal::inAttack(Animal* opponent, sf::Time dt)
{
 if (opponent == nullptr)
 {
     if(etat_animal_ != Idle)
     {
        etat_animal_ = Idle;
     }
 }
 else if (this->isAntworker_ && opponent->isAntworker_){}
 else{
     if (isEnemy(opponent)){

         if(etat_animal_ == Idle)
         {
             etat_animal_ = Attack;
         }
         if (etat_animal_ == Attack)
         {
             counter_fight_ +=dt;
//             std::cerr << "counter :" << counter_fight_.asSeconds() << std::endl;
             if (counter_fight_.asSeconds() >= getAnimal_Type_Attack_delay())
             {

//                 std::cerr << "You should escape now" << std::endl;
                 counter_fight_ = sf::Time::Zero;
                 etat_animal_ = Escaping;
             }
             impact_of_combat_on_opponent(opponent);
         }
     }
 }
}
//void Animal::inAttack(AntWorker* opponent_is_an_antworker, sf::Time dt)
//{

//}


void Animal::impact_of_combat_on_opponent(Animal* opponent)
{

    //    std::cerr << "combat" << std::endl; ->for debug
    if(opponent->pt_de_vie_ < getAnimal_Type_strength() or opponent->isAntworker_)
    {
        if(opponent->isAntworker_){
            this->etat_animal_ = Idle;
        }
        opponent->pt_de_vie_ = 0;


    }

//    if(pt_de_vie_ <opponent->getAnimal_Type_strength())
//    {
//        pt_de_vie_ =0;
//    }
    else
    {
        opponent->pt_de_vie_ -= getAnimal_Type_strength();
//        pt_de_vie_ -= opponent ->getAnimal_Type_strength(); --> as the other animal will do the exact same
    }
}

int Animal::getPt_vie() const
{
    return pt_de_vie_;
}
void Animal::setPt_vie(const int& new_pt_vie)
{
    pt_de_vie_ = new_pt_vie;
}
void Animal::setIsAntWorker()
{
    isAntworker_ = true;
}

//animal_type Animal::getAnimalType() const
//{
//    return is_a_;
//}

Animal::~Animal(){}
