/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
//------

#include <vector>
#include <SFML/Graphics.hpp>

#include <Interface/Drawable.hpp>
#include <Interface/Updatable.hpp>

#include "FoodGenerator.hpp"

#include "Utility/Types.hpp"
#include <unordered_map>


class Food;
class Animal;
class Anthill;
class ToricPosition;
class Pheromone;
class Termite;
class Ant;
//enum animal_type{antworker, antsoldier, termite, undefined};//not really useful anymore

//class FoodGenerator;

typedef std::vector<Animal*> lesAnimaux; //typedef pour augmenter la lisibilité
typedef std::vector<Food*> laNourriture;
typedef std::vector<Anthill*> lesAnthills;
typedef std::vector<Pheromone*> lesPheromones;

class Environment: private Drawable, private Updatable{

public:
    //Methodes
    void addAnimal(Animal*); //void (et pas const, car on va modifier l'objet courant)
    void addFood(Food*);
    void addAnthill(Anthill*);
    void addPheromone(Pheromone*);
    Food* getClosestFoodForAnt(ToricPosition const& position) const;
    Anthill* getAnthillForAnt(ToricPosition const& position, Uid anthillId) const;
    Animal* getClosestEnemyforAnimal(const Animal *current_animal) const; //ADDED TO CODE FOR PART 4
    void update(sf::Time dt) override; ////faire évoluer les animaux de la faune ici:
    void drawOn(sf::RenderTarget& targetWindow) const override; ////faire dessiner les animaux/ nourriture de la faune ici: !!!void might not be correct!
    void reset(); //DEF deletes all food and animals of an ecosystem.
    //constructeur/Destructeurs
    bool togglePheromoneDisplay();
    bool getAuthorization_to_draw_pheromones() const;
    Quantities getPheromoneQuantitiesPerIntervalForAnt(
      const ToricPosition &position,
      Angle direction_rad,
      const Intervals &angles
    );
    std::vector<std::string> getAnthillsIds() const; //to modify
    lesAnimaux getFaune() const;

    Environment(const Environment&) = delete; //--> forbid copy!
    Environment& operator=(Environment const&) = delete;
    Environment() = default; //Was added ?!? but pointers dont need initialization?!
    std::unordered_map<std::string, double> fetchData(const std::string &title) const;
    double getTemperature() const;
    //double animal_counter(animal_type a_t_) const;

    //Q2.3 ????
    //--> Idea?! Destructeur that also destroys memory allocation of Animal and Food alongside the memory used by the environment!
    //Alluation dynamique "ptr= new type;" et liberation de la memoire "delete ptr; ptr = nullptr;"
    //--> simultaneously -1 Animal in Faune.
    //Question: How to do that?!
    ~Environment(); //Fait appel à reset().

private:
    bool pheromones_should_be_drawn_ = true;
    //--> je n'ai pas encore décidé si je veux faire private/ protected
        //Attributs
        lesAnimaux faune_; // DEF FAUNE: désigne l'ensemble des espèces animales présentes dans un espace géographique ou un écosystème déterminé
        laNourriture food_source_;
        lesAnthills anthills_;
        lesPheromones pheromones_;
        //@ faune_, food_source_, anthills_, pheromones_ can be put into a new class: e.g. class Collections_d_Objets
        FoodGenerator food_generated_;
        double temperature_ = 20;



};
