/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include "Positionable.hpp"
#include "Utility/Types.hpp"
#include <SFML/Graphics.hpp>
#include "Interface/Drawable.hpp"

class Food: public Positionable, private Drawable {
    //nouvelles fonctions:
private:
    Quantity quantite_disponible_;
    static int foodCounter_;

public:
    Food(const ToricPosition&, const Quantity&); //ADAPT const if needed!,
    // but i think that both Positionable and ToricPosition have their own constructors:)
    Quantity takeQuantity(Quantity quantity_want_taken);
    void drawOn(sf::RenderTarget& target) const override; ////--> code later!
    static int getFoodCounter();




    //masquages:
};
