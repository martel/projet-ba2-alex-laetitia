#ifndef ANTHILL_HPP
#define ANTHILL_HPP
#pragma once
#include "Environment.hpp"
#include "Positionable.hpp"
#include "Ant.hpp"

typedef std::vector<Ant*> Population_Fourmie ;
class Anthill: public Positionable, protected Drawable,
                protected Updatable /*, protected Population_Fourmie*/
{ //laNourriture might have to be Food or not included at all --> i took it away!

private:
//    laNourriture stock_nourriture_;
    Quantity stock_nourriture_;
    sf::Time compteur_new_fourmie_;
    Uid identifiant_;
public:
    Quantity getStock_nourriture() const;
    void setNourriture(const Quantity&);
    Uid getAnthillId(); //<- I added this! cause it seemed useful for Environment::getAnthillForAnt
    Anthill(const ToricPosition& pos, const Quantity& initial_stock_food = 5.5); /*this was chosen at randomXDDD)*/
    void drawOn(sf::RenderTarget& target) const override;
    void update(sf::Time dt) override;


};



#endif // ANTHILL_HPP
