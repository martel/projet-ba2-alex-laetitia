/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "Positionable.hpp"

Positionable::Positionable(const ToricPosition& pos) : position_(pos){}
ToricPosition Positionable::getPosition() const
{
    return position_.toVec2d();
}
void Positionable::setPosition(const ToricPosition& position)
{
    position_=(position);
}
std::ostream& Positionable::display(std::ostream& out) const{
    out << '[' << position_.x() << ", " << position_.y() <<  ']';
    return out;
}
std::ostream& operator<<(std::ostream& out, Positionable const& pos)
{
    return pos.display(out);
}
