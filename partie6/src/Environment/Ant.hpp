#ifndef ANT_HPP
#define ANT_HPP
#pragma once

#include "Animal.hpp"
#include "Utility/Types.hpp"
#include "Positionable.hpp"

class Termite;
//enum ANT_TYPE {Worker, Soldier};
class Ant : public Animal{

public:
    /*!
     * @brief create ant with direct control over the start value of some of its attributes
     *
     * @param position
     * @param anthill id
     * @param health points
     * @param life expectancy
     */
    Ant(const ToricPosition& position, const Uid& fourmiliere, int health_point, int life_expectancy);
    /*!
     * @brief create ant with default values for health points and life expectancy
     *
     * @param position
     * @param anthill id

     */
    Ant(const ToricPosition& position, const Uid& fourmiliere);
    /*!
     * @brief does not do anything but is redefined in the ant subclasses for the counter
     */
    ~Ant() = default;
    /*!
     * @brief draw an ant sprite
     *
     * draws the max smell range if debug mode active
     *
     * displays rotation probs if probaDebug mode active
     *
     * @param uses the simulation window
     */
    void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief tells an ant if another animal is an enemy uses polymorphism (isEnemyDispatch) also verifies whether the other animal is dead.
     *
     * @param other animal
     *
     * @return whether the other animal is an enemy of the current Ant.
     */
    bool isEnemy(Animal const* animal) const override;
    /*!
     * @brief used by is Enemy
     *
     * @param termite
     *
     * @return termite is the enemy of every ant.
     */
    bool isEnemyDispatch(Termite const* autre_termite) const override;
    /*!
     * @brief used by isEnemy
     *
     * @param other ant
     *
     * @return whether the other ant is the enemy of the current ant.
     */
    bool isEnemyDispatch(Ant const* autre_fourmi) const override;

protected:
    /*!
     * @brief getSpeed (overrides Animal::getSpeed() )
     *
     * @return giving the default speed of an Ant
     */
    double getSpeed() const override;
    /*!
     * @brief getAnthillId_fourmie
     *
     * @return gets Anthill Identifier from current ant
     */

    Uid getAnthillId_fourmie() const;
    /*!
     * @brief add pheromones to enironment
     *
     * calls getLast_pheromone_Position() to spread pheromones continuously in between
     * the ant's "steps" (the dx by which it moves from simulation step to simulation step)
     *
     */
    void spreadPheromone();
    /*!
     * @brief a redefinition of move that calls spreadpheromone
     *
     * @param time from last update
     */
    void move(sf::Time const dt) override;
    /*!
     * @brief compute probabilities to turn towards a set of given directions. Is based on the amount of pheromones in each direction.
     *
     * @return the probablities to turn into a certain direction based on an interval (area).
     */
    RotationProbs computeRotationProbs() const override;
    /*!
     * @return position of the last Pheromone created
     */
    ToricPosition getLast_Pheromone_position();
    /*!
     * @brief used in debugging mode to show the rotation probabilities in all possible directions
     * notice: first designates the intervals of angles, whereas second the probabilities for each angle. (it must be normalized).
     *
     * @param target window
     * @param vector containing the intervals (angles)
     */
    void display_rot_probs(sf::RenderTarget& target,
                           std::vector<Angle> intervals = { -180, -100, -55, -25, -10, 0, 10, 25, 55, 100, 180 }) const;
    /*!
     *  @return default Ant attack delay.
     */
    double getAnimal_Type_Attack_delay() const override;

private:
    Uid fourmiliere_;
    ToricPosition Last_Pheromone_position_; /*--> no need cause an Ant is a positionable => getPosition:))*/

};


#endif // ANT_HPP
