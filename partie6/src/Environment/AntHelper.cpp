#include "AntHelper.hpp"
#include "Utility/Utility.hpp"
#include "Application.hpp"
#include "Food.hpp"
int AntHelper::anthelper_counter_(0);

AntHelper::AntHelper(const ToricPosition &position, const Uid& ant_hill) :
    Ant(position, ant_hill, getAppConfig().ant_helper_hp, getAppConfig().ant_helper_lifespan)
{
    ++anthelper_counter_;
    setIsAntWorkerorAntHelper();
}
sf::Texture& AntHelper::getTexture() const
{
    return getAppTexture(getAppConfig().ant_helper_texture);
}
int AntHelper::getAnimal_Type_strength() const
{
    return getAppConfig().ant_helper_strength;
}
void AntHelper::drawOn(sf::RenderTarget& target) const
{
        Ant::drawOn(target);
}
void AntHelper::update(sf::Time dt)
{
        Animal::update(dt);
        Food* closestFood(getAppEnv().getClosestFoodForAnt(getPosition()));
        if (closestFood != nullptr)
        {
            if((closestFood)->isPoisoned()
                    && (toricDistance(closestFood->getPosition(),getPosition())<= getAppConfig().ant_max_perception_distance))
            {
                closestFood->takeQuantity(closestFood->getQuantityDisponible());

            }
        }

}
int AntHelper::getAntHelperCounter()
{
    return anthelper_counter_;
}
AntHelper::~AntHelper()
{
    --anthelper_counter_;
}
