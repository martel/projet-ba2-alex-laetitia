/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include <utility>
#include "Positionable.hpp"
#include "Utility/Types.hpp"
#include "Environment.hpp"

enum Etat{Attack, Idle , Escaping};
class Termite;
class Ant;
class AntWorker;
class Animal: public Positionable, protected Updatable, protected Drawable {

public:
    //CONSTUCTORS
    /*!
     * @brief create animal with direct control over the start value of some of its attributes
     *
     * @param position
     * @param health points
     * @param life expectancy
     */
    Animal(const ToricPosition& pos, int pt_vie , int esp_vie);
    /*!
     * @brief create animal at a chosen position, it uses other constructor of Animal.
     *
     * @param positon at which Animal should be constructed
     */
    Animal(const ToricPosition& pos);

    // Getters of attributs & default values for attributes
    /*!
     * @brief Get the Speed from the resource folder for each animal (it will be overwritten)
     */
    virtual double getSpeed() const = 0;
    Angle getDirection() const ;
    /*!
     * @brief Get the Texture from the resource folder for each animal (it will be overwritten)
     */
    virtual sf::Texture& getTexture() const = 0;
    /*!
     * @brief Get the Strength for each animal from the resource folder for each animal (it will be overwritten)
     */
    virtual int getAnimal_Type_strength() const = 0;
    /*!
     * @brief Get the Attack delay for each animal from the resource folder for each animal (it must be overwritten)
     */
    virtual double getAnimal_Type_Attack_delay() const = 0;
    /*!
     * @brief useful know if we need to delete the animal
     *
     * @return whether the current animal is dead (meaning has no health points or no life points)
     */
    bool isDead() const;
    /*!
     * @brief draw the visual rendition of the simulated animal
     * @param uses the graphical interface
     *
     * calls a polymorphic member function, getTexture() that tells it which sprite to use
     *
     * is called by all drawOn functions of animal subclasses, with specific differences
     *
     * displays hp, current direction and max sight range in debug mode
     *
     */
    virtual void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief know how likely an animal is to turn in a particular direction
     */
    virtual RotationProbs computeRotationProbs() const;
    /*!
     * @brief calls move, compute_rotation_probs, decreases life expectancy and makes the ant attack
     * if an ennemy is in range
     *
     * @param time from last update
     *
     */
    void update(sf::Time dt) override; //used to udate counter & comput rotation
    /*!
     * @brief know if a given animal is an enemy of current object uses polymorphism (isEnemyDispatch) to verify the type of the respective animals.
     *
     * @return whether the current animal and the entity animal are enemies.
     */
    virtual bool isEnemy(Animal const* entity) const = 0;
    /*!
     * @brief used by isEnemy
     *
     * @return whether the termite is an enemy of the current animal.
     */
    virtual bool isEnemyDispatch(Termite const* other) const = 0;
    /*!
     * @brief used by isEnemy
     *
     * @return whether the Ant is an enemy of the current animal.
     */
    virtual bool isEnemyDispatch(Ant const* other) const = 0;
    /*!
     * @brief gets the health points of the animal
     *
     * @return healthpoints of animal
     */

    int getPt_vie() const;

    /*!
     * @brief modifies the health points of an animal, eg. to reduce it after/during a fight
     */

    void setPt_vie(const int& new_pt_vie);

    Etat getEtat_animal() const;
    /*!
     * @brief destroy an animal, must be polymorphic for the animal counters used by stats
     */
    virtual ~Animal() = default;



protected:
    /*!
     * @brief change position linearly
     *
     * @param delta of time
     *
     */
    virtual void move(sf::Time const dt);
    /*!
     * @brief set direction to chosen value
     *
     * @param angle in rad
     *
     */
    void setDirection(const Angle&);
    /*!
     * @brief change animal state between idle, attacking and fleeing
     *
     * @param new state
     *
     */
    void setEtat_animal(const Etat& nouveau_etat);

    /*!
     * @brief change the state of the animal appropriately according to animal type, isEnemy and isDead.
     *        calls impact_of_combat_on_opponent()
     *        CONCEPTION: Each Attacking animal will remove life points from the opponent every dt as long as they havent been in combat longer than their default attack delay.
     *        CONCEPTION: if Antworkers /Anthelpers are involved in a combat they will (like the other animals) remain in attack mode for the duration of their attack delay, although they will not cause any damages to the opponent.
     *        it takes into account if the pointer is a nullptr
     *        if the Animals are enemies (and not two Antworkers/ Anthelpers) and are in eachothers perception radius, their Etat_animal will become Attack.
     *
     */
    void inAttack(Animal* opponent, sf::Time dt);

    /*!
     * @brief the current Animal will take away life points from its opponent according to its default strength.two Animals will combat eachother by default they will both lose points from their lifespan according to their species.
     *        uses polymorphism for the strength of each animal.
     *
     */
    void impact_of_combat_on_opponent(Animal* opponent);
protected:
    /*!
     * @brief For all Animals except AntWorkers or AntHelpers their attribute will remain false.
     */
    void setIsAntWorkerorAntHelper();

private:
    Angle direction_;
    int pt_de_vie_;
    int life_expectancy_;
    Etat etat_animal_;
    sf::Time counter_since_last_rotation_;
    sf::Time counter_fight_;
    bool isAntworker_orAntHelper_;
};
