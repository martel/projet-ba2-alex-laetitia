/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include "Positionable.hpp"
#include "Utility/Types.hpp"
#include <SFML/Graphics.hpp>
#include "Interface/Drawable.hpp"
#include "Interface/Updatable.hpp"

class Food: public Positionable, private Drawable, protected Updatable {
public:
    /*!
     * @brief constructs an instance of food
     *
     * @param position
     * @param nutriment quantity
     * @param where the food is posioned. By default the foods are healthy.
     */
    Food(const ToricPosition&, const Quantity&, const bool& ispoisoned = false);
    /*!
     * @brief simulate food being taken away by something, the amount of food contained in the food source
     * diminishes (cannot become negative)
     *
     * @param quantity we want to take away
     */
    Quantity takeQuantity(Quantity quantity_want_taken);
    /*!
     * @brief draw a food sprite
     *
     * @param target Window
     */
    void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief Getter of static element counting all food elements
     *
     * @return Total Foods on Environment
     */
    static int getFoodCounter();
    /*!
     * @brief Getter of static element counting all poisoned food elements
     *
     * @return Total Poisoned Foods on Environment
     */
    static int getPoisonedFoodCounter();
    /*!
     * @brief assesses whether the current Food is poisoned or not
     *
     * @return true if the food is poisoned.
     */
    bool isPoisoned() const;
    /*!
     * @brief turn current food to the poison state of parameter
     *
     * @param state of poison we want to affect
     */
    void set_isPoisoned(bool isPoisoned);
    /*!
     * @brief Getter
     *
     * @return Quantity of current food
     */
    Quantity getQuantityDisponible() const;
    /*!
     * @brief Getter. Texture depends on whether the food is poisoned or not
     *
     * @return gets respective texture of food from source file
     */
    std::string getTexture() const;
    /*!
     * @brief Getter of static element counting all food elements
     *
     * @param dt elapsed time
     */
    void update(sf::Time dt) override;
    /*!
     * @brief destructor that decrements both foodcounter and poisonedfoodcounter
     */
    ~Food();
    static int foodCounter_;
    static int poisonedfoodCounter_;

private:
    Quantity quantite_disponible_;
    bool isPoisoned_;
    sf::Time counter_since_generation_ = sf::Time::Zero;
};
