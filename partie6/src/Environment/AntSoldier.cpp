#include "AntSoldier.hpp"
#include "Utility/Utility.hpp"
#include "Application.hpp"
//AntSoldier::AntSoldier(const ToricPosition &position, const Uid& ant_hill) :
//    Ant(position, ant_hill, getAppConfig().ant_soldier_hp, getAppConfig().ant_soldier_lifespan, antsoldier)
//{}
int AntSoldier::antsoldier_counter_(0);

AntSoldier::AntSoldier(const ToricPosition &position, const Uid& ant_hill) :
    Ant(position, ant_hill, getAppConfig().ant_soldier_hp, getAppConfig().ant_soldier_lifespan)
{
++antsoldier_counter_;
}

sf::Texture& AntSoldier::getTexture() const
{
    return getAppTexture(getAppConfig().ant_soldier_texture);
}

int AntSoldier::getAnimal_Type_strength() const
{
    return getAppConfig().ant_soldier_strength;
}
void AntSoldier::drawOn(sf::RenderTarget& target) const
{

        Ant::drawOn(target);
}

int AntSoldier::getAntSoldierCounter(){
    return antsoldier_counter_;
}

AntSoldier::~AntSoldier(){--antsoldier_counter_;}
