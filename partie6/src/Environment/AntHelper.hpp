#ifndef ANTHELPER_HPP
#define ANTHELPER_HPP
#pragma once
#include "Ant.hpp"
class AntHelper: public Ant{ // the aim of the AntHelper is to eliminated the rotten foods/ poison from the area.
//notice that rotten food can spread.
public:
    /*!
     * @brief create anthelper
     *
     * @param position
     * @param anthill id
     *
     */
    AntHelper(const ToricPosition& position, const Uid& ant_hill);
    sf::Texture &getTexture() const override;
    int getAnimal_Type_strength() const override;
    /*!
    * @brief draw an antHelper sprite
    *
    * draws the max smell range if debug mode active
    *
    * displays rotation probs if probaDebug mode active
    *
    * @param uses the simulation window
    */
    void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief calls Animal::update and eliminates all poisoned foods
     *
     * @param time from last update
     *
     */
    void update(sf::Time dt) override;
    static int getAntHelperCounter();
    ~AntHelper();

private:
    static int anthelper_counter_;
//    sf::Time counter_poison_distruction_ = sf::Time::Zero;
};
#endif // ANTHELPER_HPP
