/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include <SFML/Graphics.hpp>
#include "Utility/Utility.hpp"
#include "Environment.hpp"
#include "Animal.hpp"
#include "Utility/Constants.hpp"
#include "Random/Random.hpp"
#include "Random/RandomGenerator.hpp"
#include "Ant.hpp"
#include "Termite.hpp"
#include "Application.hpp"


Angle Animal::getDirection() const {
    return direction_;
}

void Animal::setDirection(const Angle& alpha){
    direction_ = alpha;
}

bool Animal::isDead() const {
    return ((pt_de_vie_<=0) or (life_expectancy_<=0));
}

void Animal::drawOn(sf::RenderTarget& target) const{

    auto const Animal_sprite = buildSprite((getPosition().toVec2d()),/*size: */ 30.0 , getTexture(), getDirection()/DEG_TO_RAD);
    target.draw(Animal_sprite);
    if (isDebugOn()){
        auto const text = buildText(to_nice_string(pt_de_vie_), ((getPosition().toVec2d()) + Vec2d(0, 25)), getAppFont(), 20, sf::Color::Red);
        target.draw(text);
        sf::VertexArray ligne(sf::PrimitiveType::Lines, 2);
        ligne[0]={(getPosition().toVec2d()), sf::Color::Black};
        ligne[1]={((getPosition().toVec2d())+((Vec2d::fromAngle(getDirection())) * 60.0) /* 60 is an arbitrary number*/), sf::Color::Black};
        target.draw(ligne);
        auto const cercle_enemy_sensor = buildAnnulus(getPosition().toVec2d(), getAppConfig().animal_sight_distance, sf::Color::Red, 5);
            target.draw(cercle_enemy_sensor);
    }
}

Animal::Animal(const ToricPosition& pos, int pt_vie, int esp_vie)
    : Positionable(pos), direction_(uniform(0.0, TAU)), pt_de_vie_(pt_vie), life_expectancy_(esp_vie),
      etat_animal_(Idle),  counter_since_last_rotation_(sf::Time::Zero), counter_fight_(sf::Time::Zero), isAntworker_orAntHelper_(false)
{}

Animal::Animal(const ToricPosition& pos)
    : Animal(pos, 1, getAppConfig().animal_default_lifespan)
{

}


void Animal::move(sf::Time dt){
    if (etat_animal_ != Attack){
        auto dx = getSpeed()*(Vec2d::fromAngle(getDirection())) * dt.asSeconds();
        setPosition(getPosition() + ToricPosition(dx));
    }
}
RotationProbs Animal::computeRotationProbs() const{
    RotationProbs RP;
    RP.first = { -180, -100, -55, -25, -10, 0, 10, 25, 55, 100, 180 };
    RP.second = {0.0000,0.0000,0.0005,0.0010,0.0050,0.9870,0.0050,0.0010,0.0005,0.0000,0.0000};
    return RP;
}

void Animal::update(sf::Time dt){
    if (counter_since_last_rotation_ > sf::seconds(getAppConfig().animal_next_rotation_delay)){
     counter_since_last_rotation_ = sf::Time::Zero;
     RotationProbs rot_prob(this->computeRotationProbs());
     std::piecewise_linear_distribution<> dist(rot_prob.first.begin(), rot_prob.first.end(), rot_prob.second.begin());

     setDirection(getDirection()+(dist(getRandomGenerator())*DEG_TO_RAD));
    }
    counter_since_last_rotation_+=dt;
    --life_expectancy_;
    inAttack(getAppEnv().getClosestEnemyforAnimal(this),dt);
    move(dt);

}
Etat Animal::getEtat_animal() const
{
    return etat_animal_;
}

void Animal::setEtat_animal(const Etat& nouveau_etat){
    etat_animal_ = nouveau_etat;
}

void Animal::inAttack(Animal* opponent, sf::Time dt)
{
 if (opponent == nullptr)
 {
     if(etat_animal_ != Idle)
     {
        etat_animal_ = Idle;
     }
 }
 else if (this->isAntworker_orAntHelper_ && opponent->isAntworker_orAntHelper_){}
 else if (isEnemy(opponent)){

         if(etat_animal_ == Idle)
         {
             etat_animal_ = Attack;
         }
         if (etat_animal_ == Attack)
         {
             counter_fight_ +=dt;
             if (counter_fight_.asSeconds() >= getAnimal_Type_Attack_delay())
             {
                 counter_fight_ = sf::Time::Zero;
                 etat_animal_ = Escaping;
             }
             impact_of_combat_on_opponent(opponent);
         }
     }
}

void Animal::impact_of_combat_on_opponent(Animal* opponent)
{
    if(opponent->pt_de_vie_ < getAnimal_Type_strength())
    {
        opponent->pt_de_vie_ = 0;
    }

    else
    {
        opponent->pt_de_vie_ -= getAnimal_Type_strength();
    }
}

int Animal::getPt_vie() const
{
    return pt_de_vie_;
}
void Animal::setPt_vie(const int& new_pt_vie)
{
    pt_de_vie_ = new_pt_vie;
}
void Animal::setIsAntWorkerorAntHelper()
{
    isAntworker_orAntHelper_ = true;
}
