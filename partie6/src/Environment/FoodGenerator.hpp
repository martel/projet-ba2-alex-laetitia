/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include <SFML/Graphics.hpp>
#include "Interface/Updatable.hpp"


class FoodGenerator : private Updatable {
private:

    sf::Time compteur_temps_;

public:
    /*!
     * @brief increment the timer since food was generated and generate a random amount after a set time limit
     */
    void update(sf::Time dt) override; //changes the time in the compteur_temps, void car on change l'attribut directement
    /*!
     * @brief default constructor initializing the counter.
     */
    FoodGenerator();
    ~FoodGenerator() = default;
};
