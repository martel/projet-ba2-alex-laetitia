#include "Pheromone.hpp"
#include "Application.hpp"
#include "Utility/Utility.hpp"


int Pheromone::pheromoneCounter_(0);

Pheromone::Pheromone(const ToricPosition& pos, const Quantity& Quant_pheromone)
    : Positionable(pos), Quant_pheromone_(Quant_pheromone), couleur_pheromone_(sf::Color::Green)
{

}
void Pheromone::update(sf::Time dt){
    couleur_pheromone_.a /= 1.01;
    Quant_pheromone_ -= (dt.asSeconds() * getAppConfig().pheromone_evaporation_rate);
}

void Pheromone::drawOn(sf::RenderTarget &target) const {

    if (getAppEnv().getAuthorization_to_draw_pheromones()){
        auto const Pheromone_sprite = buildCircle((getPosition().toVec2d()),5.0, couleur_pheromone_);
        target.draw(Pheromone_sprite);
    }

}

bool Pheromone::isNegligible() const
{
    return (Quant_pheromone_ < getAppConfig().pheromone_threshold);
}

Quantity Pheromone::getQuantity() const
{
    return Quant_pheromone_;
}

int Pheromone::getPheromoneCounter()
{
    return pheromoneCounter_;
}

