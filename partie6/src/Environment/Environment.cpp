/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "Environment.hpp"
#include "Animal.hpp"
#include "Food.hpp"
#include "Anthill.hpp"
#include "Pheromone.hpp"
#include "FoodGenerator.hpp"
#include "Application.hpp"
#include "Random/RandomGenerator.hpp"
#include <cmath>
#include "Utility/Utility.hpp"
#include "Config.hpp"
#include <unordered_map>
#include <string>
#include "AntSoldier.hpp"
#include "AntWorker.hpp"
#include "AntHelper.hpp"
#include "Termite.hpp"

typedef std::vector<Animal*> lesAnimaux;
Environment::Environment() : temperature_(getAppConfig().temperature_initial), moles_co2_(getAppConfig().environment_initial_moles_co2)
{}

void Environment::addAnimal(Animal* animal_being_added){
    faune_.push_back(animal_being_added);
}
void Environment::addFood(Food* food_being_added){
    food_source_.push_back(food_being_added);
}
void Environment::addAnthill(Anthill *anthill_being_added){
    anthills_.push_back(anthill_being_added);
}
void Environment::addPheromone(Pheromone *pheromone_being_added){
    pheromones_.push_back(pheromone_being_added);
}
Food* Environment::getClosestFoodForAnt(ToricPosition const& position_fourmie) const
{
    if (food_source_.empty()){
        return nullptr;
    }
    else {
        if (closestFromPoint(position_fourmie, food_source_) != nullptr && toricDistance(closestFromPoint(position_fourmie, food_source_)->getPosition(), position_fourmie) <= getAppConfig().ant_max_perception_distance)
        {
            return closestFromPoint(position_fourmie, food_source_);
        }
        else
        {
            return nullptr;
        }
    }

}
Anthill* Environment::getAnthillForAnt(ToricPosition const& position, Uid anthillId) const
{
    if (anthills_.empty()){
        return nullptr;
    }
    else {
        if (closestFromPoint(position, anthills_) !=nullptr && ((toricDistance(closestFromPoint(position, anthills_)->getPosition(), position) <= getAppConfig().ant_max_perception_distance))
                && (closestFromPoint(position,anthills_)->getAnthillId() == anthillId)){
            return closestFromPoint(position, anthills_);
        }
        else
        {
            return nullptr;
        }
    }



}
Animal* Environment::getClosestEnemyforAnimal(Animal const* current_animal) const
{
    if (faune_.size() == 1){
        return nullptr;
    }
    double min_distance (sqrt(2*pow(getAppConfig().simulation_size,2)));
    Animal* Enemy(nullptr);
    for (auto& animal : faune_)
    {   if (animal == nullptr) continue;
        if ((toricDistance(animal->getPosition(), current_animal->getPosition()) < min_distance) &&
                (current_animal->isEnemy(animal)))
        {
            min_distance = toricDistance(animal->getPosition(), current_animal->getPosition());
            if (min_distance <= getAppConfig().animal_sight_distance)
            {
                Enemy = animal;
            }
        }
    }
    return Enemy;

}
void Environment::update(sf::Time dt){
    food_generated_.update(dt);
        for (auto &animal: faune_){
            if (animal->isDead()){
                delete animal;
                animal = nullptr;
            }
            else {
                animal->update(dt);
            }
    }
    faune_.erase(std::remove(faune_.begin(), faune_.end(), nullptr), faune_.end());
    for (auto& food: food_source_){
        food->update(dt);
        if((food->getQuantityDisponible())<=0){
            delete food;
            food=nullptr;
        }
    }
    food_source_.erase(std::remove(food_source_.begin(), food_source_.end(), nullptr), food_source_.end());
    for (auto& food: food_source_) food->update(dt);
    for (auto& anthill: anthills_){
        anthill->update(dt);
    }
    for (auto& pheromone: pheromones_){
        pheromone->update(dt);
        if (pheromone->isNegligible()){
            delete pheromone;
            pheromone = nullptr;
        }
    }
    pheromones_.erase(std::remove(pheromones_.begin(), pheromones_.end(), nullptr), pheromones_.end());

    setNewTemperature(calculateNewTemperature(calculate_total_Co2()));
    moles_co2_ = calculate_total_Co2();
}
void Environment::drawOn(sf::RenderTarget& targetWindow) const{
    for (auto& animal: faune_) animal->drawOn(targetWindow);
    for (auto& food: food_source_) food->drawOn(targetWindow);
    for (auto& anthill: anthills_) anthill->drawOn(targetWindow);
    for (auto& pheromone: pheromones_)  pheromone->drawOn(targetWindow);

}
void Environment::reset(){
    reset_container(faune_);
    reset_container(food_source_);
    reset_container(anthills_);
    reset_container(pheromones_);

    faune_.clear();
    food_source_.clear();
    anthills_.clear();
    pheromones_.clear();
    temperature_=getAppConfig().temperature_initial;
    moles_co2_ = getAppConfig().environment_initial_moles_co2;

}

Environment::~Environment(){
    reset();
}

bool Environment::togglePheromoneDisplay()
{
    pheromones_should_be_drawn_ = (not pheromones_should_be_drawn_);
    return pheromones_should_be_drawn_;
}

bool Environment::getAuthorization_to_draw_pheromones() const
{
    return pheromones_should_be_drawn_;
}

Quantities Environment::getPheromoneQuantitiesPerIntervalForAnt(
  const ToricPosition &position,
  Angle direction_rad,
  const Intervals &angles
        )
{
    Quantities PheromoneQuantitiesPerInterval(angles.size(), 0);
    for (auto& pheromone: pheromones_)
    {
        if ((toricDistance(position, pheromone->getPosition()) < getAppConfig().ant_smell_max_distance) and not pheromone->isNegligible())
        {
            Angle AngleWithAnt_deg_beta(angleDelta((position.toricVector(pheromone->getPosition())).angle(), direction_rad));
            AngleWithAnt_deg_beta /=DEG_TO_RAD;

            if (!angles.empty()){
                Angle closest_to_angle_with_ant_beta_prime(angles[0]);
                int index_of_closest_angle(0);
                for (size_t i(1); i < angles.size(); ++i)
                {
                    if (std::abs(AngleWithAnt_deg_beta - angles[i]) < std::abs(AngleWithAnt_deg_beta - closest_to_angle_with_ant_beta_prime))
                    {
                        closest_to_angle_with_ant_beta_prime = angles[i];
                        index_of_closest_angle=i;
                    }
                }
                PheromoneQuantitiesPerInterval[index_of_closest_angle] += pheromone->getQuantity();
                if (index_of_closest_angle == ((0) or (angles.size() - 1))){
                    PheromoneQuantitiesPerInterval[angles.size()-1] = PheromoneQuantitiesPerInterval[index_of_closest_angle];
                }
            }

        }
    }


return PheromoneQuantitiesPerInterval;
}

std::vector<std::string> Environment::getAnthillsIds() const{
    std::vector<std::string> Ids;
    for (const auto& anthill : anthills_){
        Ids.push_back("anthill #" + std::to_string(anthill->getAnthillId()));
    }
    return Ids;
}

std::unordered_map<std::string, double> Environment::fetchData(const std::string & title) const{
    std::unordered_map<std::string, double> result;
    if(title == s::GENERAL){
    result = {
       {s::WORKER_ANTS, AntWorker::getAntWorkerCounter()},
       {s::SOLDIER_ANTS, AntSoldier::getAntSoldierCounter()},
       {s::HELPER_ANTS, AntHelper::getAntHelperCounter()},
       {s::TERMITES, Termite::getTermitecounter()},
       {s::TEMPERATURE, getTemperature()}
    };
    }
    else if (title == s::FOOD){
        result = {
            {s::TOTAL_FOOD, Food::getFoodCounter()},
            {s::HEALTHY_FOOD, Food::getFoodCounter()-Food::getPoisonedFoodCounter()},
            {s::POISONED_FOOD, Food::getPoisonedFoodCounter()}
            };
    }
    else if(title == s::ANTHILLS) {
        size_t i(0);
        for (auto id : getAnthillsIds())
        {
            result.insert({id, anthills_[i]->getStock_nourriture()});
            ++i;
        }

    }


    else{
        result = std::unordered_map<std::string, double>();
        std::cerr << "You tried fetching data  (in Environment::fetchData) for a Graph whose name is not like your input "<< std::endl
                            << "Please change your title, or add an according Graph in FinalApplication::resetStats()" << std::endl;
    }
   return result;

}

double Environment::getTemperature() const
{
    return temperature_;
}
Quantity Environment::calculate_total_Co2() const
{
    Quantity totalCO2(moles_co2_ + faune_.size()*getAppConfig().animal_default_co2_output);
    totalCO2 += (Food::getFoodCounter()-Food::getPoisonedFoodCounter())*getAppConfig().food_default_co2_intake;
    totalCO2 += Food::getPoisonedFoodCounter()*getAppConfig().food_poisonedfood_co2_output;
    return totalCO2;
}
double Environment::calculateNewTemperature(const Quantity& newCo2level) const
{
    return ((((temperature_ + 273)* newCo2level/  moles_co2_))-273);
}
void Environment::setNewTemperature(const double& newTemperature)
{
    temperature_ = newTemperature;
    if (temperature_ < getAppConfig().temperature_min)
        {
            temperature_=getAppConfig().temperature_min;
        }
    if (temperature_ > getAppConfig().temperature_max)
    {
        temperature_=getAppConfig().temperature_max;
    }
}
