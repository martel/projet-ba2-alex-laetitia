/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "Food.hpp"
#include "Utility/Utility.hpp"
#include "Application.hpp"
#include "ToricPosition.hpp"
int Food::foodCounter_(0);
int Food::poisonedfoodCounter_(0);

Food::Food(const ToricPosition& pos, const Quantity& quant, const bool& ispoisoned)
    : Positionable(pos), quantite_disponible_(quant), isPoisoned_(ispoisoned){
    if (isPoisoned_){
        ++poisonedfoodCounter_;
    }
    ++foodCounter_;
}


Quantity Food::takeQuantity(Quantity quantity_want_taken) {
   if (quantity_want_taken > quantite_disponible_){
       quantity_want_taken=quantite_disponible_;
   }
   quantite_disponible_-=quantity_want_taken;
   return quantity_want_taken;
}

void Food::drawOn(sf::RenderTarget& target) const{
    Vec2d position_de_Food(getPosition().toVec2d());
    double une_taille_graphique(quantite_disponible_/2.0);
    std::string texture_de_food(getAppConfig().food_texture);

    auto const foodSprite = buildSprite(position_de_Food, une_taille_graphique, getAppTexture(getTexture()));
    target.draw(foodSprite);
    if (isDebugOn()){

        auto const text = buildText(to_nice_string(quantite_disponible_), position_de_Food, getAppFont(), 15, sf::Color::Black);
        target.draw(text);
    }
}

int Food::getFoodCounter(){
    return foodCounter_;
}
int Food::getPoisonedFoodCounter(){
    return poisonedfoodCounter_;
}
bool Food::isPoisoned() const
{
    return isPoisoned_;
}
void Food::set_isPoisoned(bool is_poisoned)
{
        isPoisoned_ = is_poisoned;
        if(isPoisoned()){
            ++poisonedfoodCounter_;
        }


}
Quantity Food::getQuantityDisponible() const
{
    return quantite_disponible_;
}
std::string Food::getTexture() const
{
    if(isPoisoned()) return getAppConfig().food_poisoned_texture;
    else {return getAppConfig().food_texture;}
}
Food::~Food(){
    --foodCounter_;
    if(isPoisoned()){
        --poisonedfoodCounter_;
     }

}
void Food::update(sf::Time dt){
    counter_since_generation_+=dt;
    if (counter_since_generation_ >= sf::seconds(getAppConfig().food_toxicity_delay) && isPoisoned_ == false)
    {
        set_isPoisoned(true);
    }
}
