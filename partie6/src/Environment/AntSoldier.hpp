#ifndef ANTSOLDIER_HPP
#define ANTSOLDIER_HPP
#pragma once

#include "Ant.hpp"

class AntSoldier: public Ant{

public:
    /*!
     * @brief create an ansoldier
     *
     * calls the ant constructor
     *
     *      *
     * @param position
     * @param anthill id
     *
     * increments the antsoldier counter
     *
     */
    AntSoldier(const ToricPosition& position, const Uid& ant_hill);
    sf::Texture &getTexture() const override;
    int getAnimal_Type_strength() const override;
    /*!
     * @brief calls ant::drawOn
     *
     * @param uses the simulation window
     */
    void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief used by fetchData to update the diferent graphs, keeps the computer from computing the
     * number of instances of too many objects too often
     */
    static int getAntSoldierCounter();
    /*!
     * @brief destructor used to decrement the instance counter for antsoldiers
     */
    ~AntSoldier();
private:
    static int antsoldier_counter_;

};



#endif // ANTSOLDIER_HPP

