#ifndef LOADER_HPP
#define LOADER_HPP

#pragma once

#include <string>



/*!
 * @brief load an environment written in the map1.map convention/"format"
 *
 * @param name of the filepath
 */
 void loadMap(std::string const& filepath);


#endif // LOADER_HPP
