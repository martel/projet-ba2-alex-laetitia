/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include "ToricPosition.hpp"


/*!
 * @brief Manage a position in a toric world
 *
 * 
 */
class Positionable
{
private:
    ToricPosition position_;
public:
    /*!
     * @brief default constructor for theclass
     */
    Positionable() = default;
    /*!
     * @brief construct a positionable with a vector (that knows in whichreferential it is)
     */
    Positionable(const ToricPosition& pos);
    ToricPosition getPosition() const;
    /*!
     * @brief modify the position of an object
     */
    void setPosition(const ToricPosition& position);
    /*!
     * @brief display a position in line vector format
     */
    std::ostream& display(std::ostream& out) const;


	// A COMPLETER
};

/*!
 * @brief calls display for a positionable object
 */
std::ostream& operator<<(std::ostream&, Positionable const&);



