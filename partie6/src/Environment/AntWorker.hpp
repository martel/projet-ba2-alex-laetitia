#ifndef ANTWORKER_HPP
#define ANTWORKER_HPP
#pragma once

#include "Ant.hpp"

class AntWorker: public Ant{
public:
    /*!
     * @brief similar to antsoldier constructor, but uses antworker specific values
     * create an anworker
     *
     * calls the ant constructor
     * @param position
     * @param anthill id
     *
     * increments the antworker counter
     */
    AntWorker(const ToricPosition& position, const Uid& ant_hill);

    sf::Texture &getTexture() const override;
    int getAnimal_Type_strength() const override;
    /*!
     * @brief calls animal::drawOn and displays the amount of food carried in debug mode
     */
    void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief calls Animal::update and redirects the ant towards closest food source if it is within
     * smelling range
     *
     * if the antworker is carrying food it will deposit it at the own anthill.
     * if the antworker is carrying poisoned food for too long the antworker will die.
     * if the antworker deposits the poisoned food at the own anthill, the stock of that anthill will be reset to 0.
     *
     * @param dt elapsed time
     */
    void update(sf::Time dt) override;
    /*!
     * @brief used by fetchdata to retrieve the number of instances at a specific time
     */
    static int getAntWorkerCounter();
    /*!
     * @brief decreases the counter
     */
    ~AntWorker();

private:
    Quantity food_carrying_ = 0.0; //si la fourmie porte pas de la nourriture = 0.0--> cad elle est sensible a la nourriture autours.
    static int antworker_counter_;
    sf::Time poisoned_food_exposure_ = sf::Time::Zero;
    bool carrying_poisoned_food_ = false;

};


#endif // ANTWORKER_HPP
