#ifndef TERMITE_HPP
#define TERMITE_HPP
#pragma once
#include "Animal.hpp"

//Add Termite to Env (incomplete class)
class Termite: public Animal{
public:
//    Termite(const ToricPosition& Pos, const int& point_vie = getAppConfig().termite_hp, const int& duree_vie = getAppConfig().termite_lifespan);
    /*!
     * @brief calls animal constructor with termite specific valuesas parameters
     */
    Termite(const ToricPosition& Pos, const int& point_vie, const int& duree_vie);
    /*!
     * @brief limit "wrong use" of constructor, less chance someone might call it with the wrong values
     */
    Termite(const ToricPosition& Pos);
    sf::Texture& getTexture() const override;
    int getAnimal_Type_strength() const override;
    double getAnimal_Type_Attack_delay() const override;
    double getSpeed() const override;
    /*!
     * @brief tells a termite if another animal is its enemy. Uses polymorphism (isEnemyDispatch) also verifies whether the other animal is dead.
     *
     * @param other animal
     *
     * @return whether the other ant is the enemy of the current ant.
     */
    bool isEnemy(Animal const* animal) const override;
    /*!
     * @brief used by isEnemy
     *
     * @param other termite
     *
     * @return termites are not enemies of eachother.
     */
    bool isEnemyDispatch(Termite const* autre_Termite) const override;
    /*!
     * @brief used by isEnemy
     *
     * @param ant
     *
     * @return ants are the enemies of all termites
     */
    bool isEnemyDispatch(Ant const* autre_fourmi) const override;
    ~Termite();
    static int getTermitecounter();
protected:
private:
    static int termite_counter_;

};



#endif // TERMITE_HPP
