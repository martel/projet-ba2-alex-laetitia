#ifndef ANTHILL_HPP
#define ANTHILL_HPP
#pragma once
#include "Environment.hpp"
#include "Positionable.hpp"
#include "Ant.hpp"

typedef std::vector<Ant*> Population_Fourmie ;
class Anthill: public Positionable, protected Drawable,
                protected Updatable
{
public:
    Quantity getStock_nourriture() const;
    /*!
     * @brief modify the amount of food contained in the anthill
     *
     * @param the amount to be added
     *
     */
    void setNourriture(const Quantity&);
    void resetNourriture();
    Uid getAnthillId() const; //<- I added this! cause it seemed useful for Environment::getAnthillForAnt
    /*!
     * @brief construct an anthill
     *
     * @param a position and a starting amount of food
     *
     */
    Anthill(const ToricPosition& pos, const Quantity& initial_stock_food = 5.5); /*this was chosen at randomXDDD)*/
    /*!
     * @brief redefinition of drawOn that uses the anthill sprite
     */
    void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief generate a new ant after a given amount of time (whether it is a worker or soldier is computed
     * using a uniform distribution)
     */
    void update(sf::Time dt) override;
    ~Anthill() = default;
private:
    Quantity stock_nourriture_;
    sf::Time compteur_new_fourmie_;
    Uid identifiant_;


};



#endif // ANTHILL_HPP
