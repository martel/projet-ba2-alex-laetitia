#ifndef PHEROMON_HPP
#define PHEROMON_HPP
#pragma once
#include "Environment.hpp"
#include "Positionable.hpp"

class Pheromone: public Positionable, public Drawable, public Updatable{
private:
    Quantity Quant_pheromone_;
    sf::Color couleur_pheromone_;
    static int pheromoneCounter_;

public:
    /*!
     * @brief construct a pheromone at given location with a given amount
     *
     * @param position
     * @param quantity of pheromones
     */
    Pheromone(const ToricPosition& pos, const Quantity&);
    /*!
     * @brief used to draw a pheromone sprite
     *
     * @param targetWindow
     */
    void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief decrease sprite opacity and pheromone quantity linearly
     *
     * @param dt elapsed time
     */
    void update(sf::Time dt) override;
    /*!
     * @brief know if the amount contained in an instance is below a set value which we consider to be the
     * threshold for it to have any effect, we can then ignore that particular pheromone source
     *
     * @return whether the pheromone is negligable or not (small enough quantity)
     */
    bool isNegligible() const ;
    Quantity getQuantity() const;
    static int getPheromoneCounter();
    /*!
     * @brief default destructor
     *
     */
    ~Pheromone() = default;
};

#endif // PHEROMON_HPP
