#include "AntWorker.hpp"
#include "Anthill.hpp"
#include "Food.hpp"
#include "Utility/Utility.hpp"
#include <cmath>
#include "Application.hpp"

int AntWorker::antworker_counter_(0);

AntWorker::AntWorker(const ToricPosition& position, const Uid& ant_hill) :
    Ant(position, ant_hill, getAppConfig().ant_worker_hp, getAppConfig().ant_worker_lifespan)
{
    setIsAntWorkerorAntHelper();
    ++antworker_counter_;
}

sf::Texture& AntWorker::getTexture() const
{
    return getAppTexture(getAppConfig().ant_worker_texture);
}
int AntWorker::getAnimal_Type_strength() const
{
    return getAppConfig().ant_worker_strength;
}
void AntWorker::drawOn(sf::RenderTarget& target) const
{
        Ant::drawOn(target);
        if (isDebugOn()){
            auto PorteFood_text = buildText(to_nice_string(food_carrying_), (getPosition().toVec2d() + Vec2d(0, 40)), getAppFont(), 20, sf::Color::Magenta);
            target.draw(PorteFood_text);
        }
}
void AntWorker::update(sf::Time dt)
{
    Animal::update(dt);
    Food* pointer_to_closest_food(getAppEnv().getClosestFoodForAnt(getPosition()));
    if ((food_carrying_ == 0.0) &&  pointer_to_closest_food!= nullptr){
        food_carrying_= pointer_to_closest_food->takeQuantity(getAppConfig().ant_max_food);
        setDirection(getDirection() + PI);
        if (pointer_to_closest_food->isPoisoned()){
            poisoned_food_exposure_ = sf::Time::Zero;
            carrying_poisoned_food_ = true;
        }
    }
    Anthill* anthill_close(getAppEnv().getAnthillForAnt(getPosition(),getAnthillId_fourmie()));
    if ((food_carrying_ != 0.0) && (anthill_close) != nullptr){
        if (carrying_poisoned_food_){
        (anthill_close)->resetNourriture();
        } else{
            (anthill_close)->setNourriture(food_carrying_);
            carrying_poisoned_food_ = false;
        }
        food_carrying_ =0.0;
    }
    if ((food_carrying_!=0.0) and carrying_poisoned_food_){
        poisoned_food_exposure_ += dt;
    }
    if (poisoned_food_exposure_ >= sf::seconds(getAppConfig().ant_worker_max_poison_exposure))
    {
        setPt_vie(0);
    }

}

int AntWorker::getAntWorkerCounter(){
    return antworker_counter_;
}

AntWorker::~AntWorker(){--antworker_counter_;}




