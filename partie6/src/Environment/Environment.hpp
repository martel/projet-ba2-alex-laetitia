/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
//------

#include <vector>
#include <SFML/Graphics.hpp>

#include <Interface/Drawable.hpp>
#include <Interface/Updatable.hpp>

#include "FoodGenerator.hpp"

#include "Utility/Types.hpp"
#include <unordered_map>


class Food;
class Animal;
class Anthill;
class ToricPosition;
class Pheromone;
class Termite;
class Ant;
//enum animal_type{antworker, antsoldier, termite, undefined};//not really useful anymore

//class FoodGenerator;

typedef std::vector<Animal*> lesAnimaux; //typedef pour augmenter la lisibilité
typedef std::vector<Food*> laNourriture;
typedef std::vector<Anthill*> lesAnthills;
typedef std::vector<Pheromone*> lesPheromones;

class Environment: private Drawable, private Updatable{

public:
    //Methodes
    /*!
     * @brief adds the pointer to an instance of a class inheriting from positionable given
     * as parameter to the corresponding vector of pointers
     */
    void addAnimal(Animal*); //void (et pas const, car on va modifier l'objet courant)
    void addFood(Food*);
    void addAnthill(Anthill*);
    void addPheromone(Pheromone*);
    /*!
     * @brief gets Closest Food to the position of the Current Ant. Uses template: closest point to.
     *
     * @param position of Ant
     *
     * @return if found returns pointer to the closest Food. Else returns nullptr.
     */
    Food* getClosestFoodForAnt(ToricPosition const& position) const;
    /*!
     * @brief gets closest Anthill to the position of the Current Ant. Uses template: closest point to.
     *
     * @param position of Ant
     *
     * @return if found returns pointer to the closest Anthill. Else returns nullptr.
     */
    Anthill* getAnthillForAnt(ToricPosition const& position, Uid anthillId) const;
    /*!
     * @brief gets closest Enemy to the position of the Current Animal.
     *
     * @param position of Animal
     *
     * @return if found returns pointer to the closest Enemy (Animal). Else returns nullptr.
     */

    Animal* getClosestEnemyforAnimal(const Animal *current_animal) const;
    /*!
     * @brief calls the redefinitions of update for the different types of objects contained in the environment
     * and removes some objects (eg. dead animals) from the corresponding vector of pointers
     *
     * notice: If the animal created first dies during a fight, while we are still iterating on the faun, we might get a nullptr and we'd want to access a nullptr. To avoid a crash, this animal must be skipped.
     *
     * @param dt elapsed time
     *
     */
    void update(sf::Time dt) override;
    /*!
     * @brief calls redefinitions of drawOn for all non nullptr elements in the vector
     * of pointers of Drawable objects.
     *
     * @param targetWindow
     */
    void drawOn(sf::RenderTarget& targetWindow) const override;
    /*!
     * @brief DEF deletes all objects of the environment and resets environment to the initial conditions (e.g. Temperature etc.).
     *
     */
    void reset();
    /*!
     * @brief change whether or not pheromones should be displayed
     */
    bool togglePheromoneDisplay();
    /*!
     * @brief know whether or not pheromones should be drawn
     *
     * @return should pheromones be drawn on environment or not
     */
    //Getters of Attributes
    /*!
     * @brief pheromones will be drawn only if drawPheromones is true
     *
     * @return whether pheromones should be drawn or not
     */
    bool getAuthorization_to_draw_pheromones() const;
    /*!
     * @brief get Pheromone Quantities Per Interval, used for ants
     *        iterates on pheromones, if they are within detection range & not negligible:
     *        find which angle in the intervals given is closest & add its amount to vector of quantities
     * @param position
     * @param direction_rad
     * @param angles
     *
     * @return vector containing quantities per interval
     */
    Quantities getPheromoneQuantitiesPerIntervalForAnt(
      const ToricPosition &position,
      Angle direction_rad,
      const Intervals &angles
    );
    std::vector<std::string> getAnthillsIds() const;
    //CONSTRUCTOR & DESTRUCTOR & COPY
    /*!
     * @brief forbid copy by deleting the copy constructor
     */
    Environment(const Environment&) = delete;
    /*!
     * @brief forbid copy that could otherwise potentially happen through the operator=
     */
    Environment& operator=(Environment const&) = delete;
    /*!
     * @brief creates Environment and initializes its attributes using the sourcefile.
     */
    Environment();
    /*!
     * @brief return environment data at a specific time to update the graphs
     *
     * @param title of the graph we want to fetch the data for
     *
     * @return unordered map containing the name of each variable and the according new value.
     */
    std::unordered_map<std::string, double> fetchData(const std::string& title) const;
    double getTemperature() const;
    /*!
     * @brief calculates total CO2 of environment (as Animals and Food release/ absorb Co2)
     *
     * @return quantity of current total Co2 of environment
     */
    Quantity calculate_total_Co2() const;
    /*!
     * @brief calculate new temperature of environment taking into account the new Co2 level.
     *        Maths: Uses ideal gas law : nRT= PV. Where P=1atm, V = cst, R= cst => (T1/n1)=(T2/n2).
     *
     * @param the new Co2
     *
     * @return new calculated temperature
     */
    double calculateNewTemperature(const Quantity& newCo2level) const;
    /*!
     * @brief set the temperature of the environment to the new temperature
     *
     * @param new temperature (calculated in calculate_total_Co2())
     */
    void setNewTemperature(const double& newTemperature);
    /*!
      * @brief destructor that calls reset, to delete the old environment.
      */
    ~Environment();

private:
    bool pheromones_should_be_drawn_ = true;
    lesAnimaux faune_;
    laNourriture food_source_;
    lesAnthills anthills_;
    lesPheromones pheromones_;
    FoodGenerator food_generated_;
    double temperature_;
    Quantity moles_co2_;



};
