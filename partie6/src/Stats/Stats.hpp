#ifndef STATS_HPP
#define STATS_HPP

#include <memory>
#include <vector>
#include "Interface/Drawable.hpp"
#include "Interface/Updatable.hpp"
#include "Utility/Vec2d.hpp"
#include <unordered_map>
#include "Graph.hpp"


class Graph;

class Stats : protected Updatable, protected Drawable{
public:
    /*!
     * @brief choose which graph is of interest
     *
     * @param new id
     */
    void setActive(int id);
    /*!
     * @return title of the current graph
     */
    std::string getCurrentTitle() const;
    /*!
     * @brief choose next graph using user commands
     */
    void next();
    /*!
     * @brief choose previous graph using user commands
     */
    void previous();
    /*!
     * @brief display a graph in simulation window
     *
     * @param targetWindow
     */
    void drawOn(sf::RenderTarget& target) const override;
    /*!
     * @brief reset all of the instance's graphs
     */
    void reset();
    /*!
     * @brief replace a (possibly hitherto nonexistent) graph with a new one
     */
    void addGraph(
      int id,
      const std::string &title,
      const std::vector<std::string> &series,
      double min,
      double max,
      const Vec2d &size
    );
    /*!
     * @brief fetch for all graphs the latest data available (the static getters for the
     * instance counters play a part here)
     *
     * increases the counter by dt
     *
     */
    void update(sf::Time dt) override;
    /*!
     * @brief create an "empty" stats instance with empty labels and graphs maps and a counter initially set to zero
     */
    Stats();


private:

    std::unordered_map<int ,std::unique_ptr<Graph>> graphs_;
    std::unordered_map<int ,std::string> labels_;
    int active_id_;
    sf::Time counter_since_last_update_;

};

#endif // STATS_HPP
