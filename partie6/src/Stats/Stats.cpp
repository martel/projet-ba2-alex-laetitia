//#include "Stats/Graph.hpp"

#include "Graph.hpp"
#include "Stats.hpp"
#include "Application.hpp"
#include <memory>
#include <vector>

void Stats::setActive(int id){
    active_id_ = id;
}

std::string Stats::getCurrentTitle() const{
    return labels_.find(active_id_)->second;
}

void Stats::next(){
    ++active_id_;
    if (active_id_ > (labels_.size() - 1)){ active_id_ = 0;
    }
}

void Stats::previous(){
    --active_id_;
    if (active_id_ < 0) {active_id_ = (labels_.size() -1);}
}

void Stats::drawOn(sf::RenderTarget& target) const{
    graphs_.find(active_id_)->second->drawOn(target);

}

void Stats::reset(){
    for (auto& it : graphs_){
        it.second->reset();
    }
}

void Stats::addGraph(
  int id,
  const std::string &title,
  const std::vector<std::string> &series,
  double min,
  double max,
  const Vec2d &size
        )
{

    graphs_[id].reset(new Graph(series, size, min, max)); labels_[id] = title;
    active_id_ = id;

}

void Stats::update(sf::Time dt){
    if (counter_since_last_update_ > sf::seconds(getAppConfig().stats_refresh_rate))
    {

        for (auto& graph : graphs_)
        {

            graph.second->updateData(counter_since_last_update_, getAppEnv().fetchData(labels_[graph.first]));

        }
            counter_since_last_update_ = sf::Time::Zero;

    }
    counter_since_last_update_ += dt;
}

Stats::Stats():
    graphs_(), labels_(), active_id_(0), counter_since_last_update_(sf::Time::Zero)

{}
