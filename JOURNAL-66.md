#Programmation Orientée-objets(SV)

##JOURNAL du groupe 66 (Laetitia Schwitter, Alexandre Martel) 

*************************************************
##09.03

- inscription du groupe 


*************************************************
##12.03

- lecture de l'énonce
- apprentissage du maniement de git

*************************************************
## 22.03
-codage de la plupart de la partie 1 (sauf toricvector et toric distance)
-erreurs de compilation

*************************************************
## 23.03
- partie 1 compile

*************************************************
## 27.03

- correction du fonctionnemnet de l'algorithme de clamping (ne construisais pas le vecteur désiré à la sortie)
- correction de toricvector (changement des boucle et de l'incrémentation utilisée pour obtenir toutes les combinaisons possible)
- correction du constructeur de copie et de la surcharge de l'opérateur de recopie = pour ToricPosition, bonne définition par défaut
- correction de la fonction display et de la surcharge de l'opérateur <<
- passage de toricTest et testPositionable
- Partie 1 finie

*************************************************
## 28/03

- passage de positionnable test
- quelques optimisations de code

*************************************************
## 30/03

- résolution des problèmes empêchant le passage de FoodTest pour les classes Food et Environment
- compile mais possiblement quelques soucis d'exécution

*************************************************
## 07/04

-choix aléatoire d'angle de direction n'est pas aléatoire
-quantité de nourriture, nbr de pt de vie et segment de direction ne s'affichent pas
-nourriture générée trop rapidement

*************************************************
## 08/04

-angle est aléatoire et autres problèmes du 07 résulus
-les sprites tournent
-takeQUantity modifié pour utiliser moins de code

*************************************************
## 09/04

-les fourmis se déplacent mais font l'hélicotère (tentatives de rotations trop fréquentes et mauvaise conversion
en radian de l'angle à ajouter->rotations trop importantes)
-modification de update afin que l'angle ajouté à la direction soit correct
-soucis de conception de la logique d'appel des fonction move et update dans animal dûe à une erreur de compréhension de l'énoncé
-update et move ne marchent pas comme on voudrait


*************************************************
## 13/04

-les fourmis avancent et tournent, content.
-modification de la hiérarchie d'appel entre lesfonctions move et update pour mieux suivre l'énoncé
-problème avec l'emplacement du fichier build de la partie 3 (gros problème)

*************************************************
## 15/04

-problème de génération des fichiers résolu

*************************************************
## 18/04

-inclusions et constructeurs de la partie 3 faits


*************************************************
## 26/04

-tests 1 à 14 passés
(les fichiers liés aux phéromones se nomment pheromon.[hpp][cpp], nous avons  modifiés les fichier tests
pour accomoder l'orthographe)

*************************************************
## 27/04

-test 15 passé

*************************************************
## 30/04
- déplacement sensoriel fonctionne présque complètement, mais on dessine beaucoup trop de phéromones, cela semble être lié à une mauvaise compréhension de la méthode spreadpheromones.

************************************************* 
## 03/05 
- problème : les fourmis ne changent pas de probabilité sur le côté gauche.
*************************************************
## 04/05

- les fourmis affichent les bonnes probabilités.
- amélioration des methodes cherchant les Anthill / Foods les plus proches.
- établisement du setting pour la partie 4. 

*************************************************
## 05/05

- partie 4.1 passe ses tests
- partie 4.2 fait sauf combat

*************************************************
## 06/05

- il reste des soucis de compréhension d'énoncé :
  1) Pour les AntWorkers meutrent-elles directement à chaque combat ou est-ce que ils se battent juste pas?
  2) mode attack --> pendant le mode attaque, les adversaires se frappent continuellement pendant leur delai d'attaque?
 
## 07/05

- résolution du seg fault lorsque le combattant crée avant meure en premier, mais pas l'inverse.
- partie 4 passe

*************************************************
## 08/05

- codé jusqu'au test 20, mais erreur de compilation (constructeur manquant)

*************************************************
## 11/05

- seg fault lors de l'ajout d'un nouvel graphe (car fetchData encore incomplet)

*************************************************
## 13/05 

- 5.1 passe
- commencement de la partie 5.2

*************************************************
## 16/05

- code compilable pour la partie 5.2 mais loadmap n'est pas reconnu.

*************************************************
## 17/05 

- discussion sur les extensions:
  Idees présentes pour une extension:
  e.g. 1) jour/ nuit : changement de speed des animaux (plus lent pendant la nuit), Temperature et la AntWorker ne travaille pas.
       2) Rotten Food et pesticide: effects sur anthill/ rest de environment on discute encore.
- description de manière propre des methodes dans le code. 

*************************************************
## 18/05

-utiliser ifstream ou stringstream dans la lecture du fichier .map

*************************************************
## 19/05 

-compile mais problème de conceptualisation de loadmap, compliqué à tester, probablement un mauvais usage de split ou une erreur dans 
la manière de lire le fichier

*************************************************
## 21/05 

-loadmap est plus correct conceptuellement (usage correct de getline et de split) mais pas reconnue (undefined reference to...)

*************************************************
## 23/05 

-renouveler la configuration Cmake a résolu le problème de référence indéfinie
-tout fonctionne

*************************************************
## 25/05 

-commenter le code

*************************************************
## 26/05 

-travail sur les extensions
-introduction de sources de nourriture empoisonnée
-nouvelle classe de fourmis semblables à des antworker devant rendre la nourriture empoisonnée comestible (AntHelper)
-changement de l'héritage de Food (protected updatable) pour lui donner une fonction update pour qu'elle devienne empoisonnée 
après un certain temps
-changement de antworker::update pour qu'elle meurent si elles transportent de la nourriture empoisonnée trop longtemps

*************************************************
## 28/05 

-mise en place d'un "cycle du co2" où le co2 est libéré par les animaux et la norriture empoisonnée et extrait de l'atmosphère
par la nourriture normale (fonctionne comme une plante)
-la température est modifiée par la quantité de co2 présente


*************************************************
## 29/05 

-changement du sprite utilisé pour anthelper





















