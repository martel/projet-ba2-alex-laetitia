#ifndef ANT_HPP
#define ANT_HPP
#pragma once

#include "Animal.hpp"
#include "Utility/Types.hpp"
#include "Positionable.hpp"


//enum ANT_TYPE {Worker, Soldier};
class Ant : public Animal{

public:
    Ant(const ToricPosition& position, const Uid& fourmiliere, int h_p = 1, int life_exp = getAppConfig().animal_default_lifespan);
    void drawOn(sf::RenderTarget& target) const;

protected:
    double getSpeed() const override;
    Uid getAnthillId_fourmie() const;
    void spreadPheromone();
    void move(sf::Time const dt) override;
    RotationProbs computeRotationProbs() const override; /*TO CODEEEE!!!*/
    ToricPosition getLast_Pheromone_position();

    void display_rot_probs(sf::RenderTarget& target,
                           Quantities intervals = { -180, -100, -55, -25, -10, 0, 10, 25, 55, 100, 180 }) const;

private:
    Uid fourmiliere_;
    ToricPosition Last_Pheromone_position_; /*--> no need cause an Ant is a positionable => getPosition:))*/

};


#endif // ANT_HPP
