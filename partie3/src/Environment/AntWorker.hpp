#ifndef ANTWORKER_HPP
#define ANTWORKER_HPP
#pragma once

#include "Ant.hpp"

class AntWorker: public Ant{
public:
    AntWorker(const ToricPosition& position, const Uid& ant_hill);
    sf::Texture &getTexture() const override;
    void drawOn(sf::RenderTarget& target) const override;
    void update(sf::Time dt) override;
private:
    Quantity food_carrying_ = 0.0; //si la fourmie porte pas de la nourriture = 0.0--> cad elle est sensible a la nourriture autours.

};


#endif // ANTWORKER_HPP
