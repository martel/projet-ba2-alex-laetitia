/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "FoodGenerator.hpp"
#include "Application.hpp"
#include "Random/Random.hpp"
#include "Food.hpp"
#include <cmath>
void FoodGenerator::update(sf::Time dt){
    //compteur
    if (compteur_temps_> sf::seconds(getAppConfig().food_generator_delta)){
        compteur_temps_ = sf::Time::Zero;
        //position de cette nourriture:
        double coord_x_random(normal((getAppConfig().simulation_size)/2, std::pow((getAppConfig().simulation_size)/4.0,2)));
        double coord_y_random(normal((getAppConfig().simulation_size)/2, std::pow((getAppConfig().simulation_size)/4.0,2)));
        ToricPosition toric(coord_x_random, coord_y_random);
        Quantity quantity_random(uniform(getAppConfig().food_min_qty,getAppConfig().food_max_qty));
        // Access à l'environment:
        getAppEnv().addFood(new Food(toric, quantity_random));
    }
    compteur_temps_ += dt;


}
FoodGenerator::FoodGenerator()
    : compteur_temps_(sf::Time::Zero){
}
