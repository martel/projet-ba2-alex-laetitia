/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "Environment.hpp"
#include "Animal.hpp"
#include "Food.hpp"
#include "Anthill.hpp"
#include "Pheromon.hpp"
#include "FoodGenerator.hpp"
#include "Application.hpp" //im not sure this goes here or in cpp.
//--> Application directory might be "src/Application.hpp" but it didnt seem to find this!
#include "Random/RandomGenerator.hpp"
#include <cmath>
#include "Utility/Utility.hpp"


//METHODS OF CLASS ENVIRONMENT
void Environment::addAnimal(Animal* animal_being_added){//same for addfood, reference and use of new to control memory allocation outside range instead of pointer which might get corrupted once outside the range of addsmth
    faune_.push_back(animal_being_added);
}
void Environment::addFood(Food* food_being_added){
    food_source_.push_back(food_being_added);
}
void Environment::addAnthill(Anthill *anthill_being_added){
    anthills_.push_back(anthill_being_added);
}
void Environment::addPheromone(Pheromone *pheromone_being_added){
//    std::cerr <<"pheromone was added" << std::endl; --> for debug
    pheromones_.push_back(pheromone_being_added);
}
Food* Environment::getClosestFoodForAnt(ToricPosition const& position_fourmie)
{
    size_t index(0);
    if (not food_source_.empty()){

        double dist_min(toricDistance(position_fourmie,(food_source_[0])->getPosition()));

        for (size_t i(1); i < food_source_.size(); ++i){
                if (toricDistance(position_fourmie,(food_source_[i]->getPosition())) < dist_min){
                dist_min = toricDistance(position_fourmie,(food_source_[i]->getPosition()));
                index = i;

            }

        }
        if (dist_min < getAppConfig().ant_max_perception_distance){
            return food_source_[index];
        }
        else return nullptr;

    }
    else return nullptr;

}
Anthill* Environment::getAnthillForAnt(ToricPosition const& position, Uid anthillId)
{
//    @QUESTION!!
    if (not anthills_.empty()){
        for (size_t i(0); i< anthills_.size(); ++i){
            if ((anthills_[i]->getAnthillId() == anthillId)
                    && (toricDistance(position,anthills_[i]->getPosition()) <= getAppConfig().ant_max_perception_distance)){
                return anthills_[i];
                /*!!!! What should it return if the Anthill is not close?! */
            }
        /* there should never be an else unless, the ants were not properly distroyed*/
        }
        return nullptr;
    }
    else return nullptr;

}
void Environment::update(sf::Time dt){
    food_generated_.update(dt);
    //std::cout << "Taille vecteur " << food_source_.size() << std::endl;
        for (auto &animal: faune_){
        if (animal->isDead()){// no need for the content of the pointer address bc reference?
            animal = nullptr; //shouldn't it be delete?!

        }
        else {
            animal->update(dt);
        }
    }
    faune_.erase(std::remove(faune_.begin(), faune_.end(), nullptr), faune_.end());
    for (auto& anthill: anthills_){
        anthill->update(dt);
    }
    for (auto& pheromone: pheromones_){
        pheromone->update(dt);
    }
//    std::cerr << "nbr pheromones : " << pheromones_.size()<< std::endl; --> for debug
    ////TO BE CODED!!!!
}
void Environment::drawOn(sf::RenderTarget& targetWindow) const{
    for (auto& animal: faune_) animal->drawOn(targetWindow); //ne plus: animal-> Animal::drawOn puisqu'on veut faire appel a la fonction DrawOn du type courant
    for (auto& food: food_source_) food->/*Food::*/drawOn(targetWindow);
    for (auto& anthill: anthills_) anthill->drawOn(targetWindow);
//    if (pheromones_should_be_drawn_){
//    if (pheromones_.empty()) std::cout<<"no pheromones detected"<<std::endl;
        for (auto& pheromone: pheromones_)
        {
            pheromone->drawOn(targetWindow);

        }
//        std::cerr<< "true" <<std::endl;
//    }
//   else {
//        std::cerr<< "false " << std::endl;
//      }
}
void Environment::reset(){
    for (auto& animal: faune_){
        delete animal;
        animal = nullptr;
    }
    for (auto& food: food_source_){
        delete food;
        food = nullptr;
    }
    for (auto& anthill: anthills_){
        delete anthill;
        anthill = nullptr;
    }
    for (auto& pheromone: pheromones_){
        delete pheromone;
        pheromone = nullptr;
    }
    faune_.clear();
    food_source_.clear();
    anthills_.clear();
    pheromones_.clear();
    //Is there a more efficient way that duplicates less code?
}

Environment::~Environment(){
    reset();
}

bool Environment::togglePheromoneDisplay()
{
    pheromones_should_be_drawn_ = (not pheromones_should_be_drawn_);
    if (pheromones_should_be_drawn_) std::cerr<<"true"<<std::endl;
    else std::cerr<<"false"<<std::endl;
    return pheromones_should_be_drawn_;
}

bool Environment::getAuthorization_to_draw_pheromones() const
{
    return pheromones_should_be_drawn_;
}

Quantities Environment::getPheromoneQuantitiesPerIntervalForAnt(
  const ToricPosition &position,
  Angle direction_rad,
  const Intervals &angles
        )
{
    Quantities PheromoneQuantitiesPerInterval(angles.size(), 0);
    for (auto& pheromone: pheromones_)
    {
        if ((toricDistance(position, pheromone->getPosition()) < getAppConfig().ant_smell_max_distance) and not pheromone->isNegligible())
        {
            Angle AngleWithAnt_deg_beta(angleDelta((position.toricVector(pheromone->getPosition())).angle(), direction_rad));
            AngleWithAnt_deg_beta /=DEG_TO_RAD;

            if (!angles.empty()){
                Angle closest_to_angle_with_ant_beta_prime(angles[0]);
                int index_of_closest_angle(0);
                for (size_t i(1); i < angles.size(); ++i)
                {
                    if (std::abs(AngleWithAnt_deg_beta - angles[i]) < std::abs(AngleWithAnt_deg_beta - closest_to_angle_with_ant_beta_prime))
                    {
                        closest_to_angle_with_ant_beta_prime = angles[i];
                        index_of_closest_angle=i;
                    }
                }
                PheromoneQuantitiesPerInterval[index_of_closest_angle] += pheromone->getQuantity();
                if (index_of_closest_angle == ((0) or (angles.size() - 1))){
//                    PheromoneQuantitiesPerInterval[0] = PheromoneQuantitiesPerInterval[index_of_closest_angle];
                    PheromoneQuantitiesPerInterval[angles.size()-1] = PheromoneQuantitiesPerInterval[index_of_closest_angle];
                }
            }

        }
    }

    //first iterate on pheromones, if they are within detection range & are not negligible, find
    //the angle, find which angle in the inetervals given ic closest & add amount to vector of
    //quantities that I want to return
return PheromoneQuantitiesPerInterval;
}
Environment.cpp
Affichage de Environment.cpp en cours...
