#include "AntSoldier.hpp"
#include "Utility/Utility.hpp"
AntSoldier::AntSoldier(const ToricPosition &position, const Uid& ant_hill) :
    Ant(position, ant_hill, getAppConfig().ant_soldier_hp, getAppConfig().ant_soldier_lifespan)
{}
sf::Texture& AntSoldier::getTexture() const
{
    return getAppTexture(getAppConfig().ant_soldier_texture);
}
void AntSoldier::drawOn(sf::RenderTarget& target) const
{
//        auto const Soldier_sprite = buildSprite((getPosition().toVec2d()),/*size: where do i find that --> 20.0 is completely arbitrary? */ 50.0 ,getAppTexture(getAppConfig().ant_soldier_texture), getDirection()/DEG_TO_RAD);
//        target.draw(Soldier_sprite);
        Ant::drawOn(target);
}
