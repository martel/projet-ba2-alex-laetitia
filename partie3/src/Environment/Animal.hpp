/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#pragma once
#include <utility>
#include "Positionable.hpp"
#include "Utility/Types.hpp"
#include "Animal.hpp"
#include "Application.hpp"
// no need to do #include "Environment.hpp" as application.hpp already contains it.

class Animal: public Positionable, protected Updatable, protected Drawable {

public:
    Animal(const ToricPosition& pos, int pt_vie = 1, int esp_vie = (getAppConfig().animal_default_lifespan));
    virtual double getSpeed() const = 0;
    Angle getDirection() const ;
    virtual sf::Texture& getTexture() const = 0;
    bool isDead();
    virtual void drawOn(sf::RenderTarget& target) const override;
    virtual void move(sf::Time const dt);
    virtual RotationProbs computeRotationProbs() const;
    void update(sf::Time dt) override; //used to udate counter & comput rotation




private:
    Angle direction_;
    sf::Time counter_since_last_rotation_;
    int pt_de_vie_;
    int life_expectancy_; //pt_de_vie & life_expectancy are private in part2.

protected:

    void setDirection(const Angle&);
};
