/*
 * POOSV 2020-21
 * @author: Laetitia Schwitter & Alexandre Martel
 */

#include "Positionable.hpp"

Positionable::Positionable() : position_(){} /* i want it to call the default constructor of the class ToricPosition */
Positionable::Positionable(const ToricPosition& pos) : position_(pos){}
ToricPosition Positionable::getPosition() const
{
    return position_.toVec2d(); //will return the coord of ToricPosition position
}
void Positionable::setPosition(const ToricPosition& position)
{
    position_=(position); //fait appel à la surcharge de l'operateur d'affectation defini en toric position.
}
std::ostream& Positionable::display(std::ostream& out) const /*const car ne modifie pas l'attribut personne */{
    out << '[' << position_.x() << ", " << position_.y() <<  ']';
    return out;
}
//externe
std::ostream& operator<<(std::ostream& out, Positionable const& pos)
{
    return pos.display(out);
}
