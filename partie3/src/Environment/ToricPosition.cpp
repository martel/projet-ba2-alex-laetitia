﻿/*
 * POOSV 2020-21
 * Marco Antognini & Jamila Sam
 */

#include "ToricPosition.hpp"
#include "../Application.hpp"
#include "Utility/Utility.hpp"
#include "../Config.hpp"
#include <cmath> // abs and fmod
#include <cassert>

ToricPosition::ToricPosition()
: ToricPosition(0.0, 0.0)
{
    // Done.
}

ToricPosition::ToricPosition(double x, double y)
: ToricPosition(Vec2d(x, y))
{
    // Done.
}

ToricPosition::ToricPosition(Vec2d const& position)
    : ToricPosition(position, Vec2d(getAppConfig().simulation_size,
                                    getAppConfig().simulation_size))
{
}

ToricPosition::ToricPosition(Vec2d const& position, Vec2d const& dims)
    : mPosition(position)
    , mWorldSize(dims)
{
    clamp();
}

Vec2d const& ToricPosition::toVec2d() const
{
    return mPosition;
}

double ToricPosition::operator[](unsigned int i) const
{
  assert(i<2);
  return mPosition[i];
}

bool ToricPosition::operator==(const ToricPosition& b) const
{
  return isEqual(x(), b.x()) and isEqual(y(), b.y());
}

ToricPosition operator+(ToricPosition a, const ToricPosition& b)
{
  a+=b;
  return a;
}
ToricPosition& ToricPosition::operator+=(const ToricPosition& b)
{
    return *this += b.mPosition ;
}

ToricPosition& ToricPosition::operator+=(const Vec2d& b)
{
  mPosition += b;
  clamp();
  return *this;
}
static double myfmod(double x, double y)
{
  x = fmod(x, y);
  if (x < 0.0) x += y;
  return x;
}
void ToricPosition::clamp()
{
    // Clamp the position inside the toric world

    auto const width  = mWorldSize.x();
    auto const height = mWorldSize.y();
    mPosition = Vec2d(myfmod(mPosition.x(), width),
                      myfmod(mPosition.y(), height));
    // while (mPosition.x < 0)          mPosition.x += width;
    // while (mPosition.x >= width)     mPosition.x -= width;
    // while (mPosition.y < 0)          mPosition.y += height;
    // while (mPosition.y >= height)    mPosition.y -= height;
}

Vec2d ToricPosition::toricVector(ToricPosition const& to) const
{
    // a === from, b === to
    // auto const a = from.toVec2d();
    // auto const b = to.toVec2d();
    auto const a = mPosition;
    auto const b = to.mPosition;

    auto const width  = mWorldSize.x();
    auto const height = mWorldSize.y();


    // Let's fix a and find the closest b possible in this world
    // There are 9 candidates:
    // - b itself
    // - b in the "world above" (shifted by { 0, height })
    // - b in the "world below" (shifted by { 0, -height })
    // - b shifted by { width, 0 }
    // - b shifted by { -width, 0 }
    // - b shifted by { width, height }
    // - b shifted by { -width, height }
    // - b shifted by { width, -height }
    // - b shifted by { -width, -height }

    auto min_b = b;
    auto min_dist = distance(min_b, a);
    for (auto i : { -1, 0, 1 }) {
        for (auto j : { -1, 0, 1 }) {
            auto const candidate = b + Vec2d{ width * i, height * j };
            auto const candidate_dist = distance(candidate, a);
            if (candidate_dist < min_dist) {
                min_dist = candidate_dist;
                min_b = candidate;
            }
        }
    }

    return min_b - a;
}

double toricDistance(ToricPosition const& from, ToricPosition const& to)
{
    return from.toricVector(to).length();
}

std::ostream& operator<<(std::ostream& out, ToricPosition const& v)
{
  out << "[" << v[0] << ", " << v[1] << "]";
  return out;
}
