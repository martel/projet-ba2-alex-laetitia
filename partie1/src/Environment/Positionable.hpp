/*
 * POOSV 2020-21
 * @author: 
 */

#pragma once
#include "ToricPosition.hpp"


/*!
 * @brief Manage a position in a toric world
 *
 * 
 */
class Positionable
{
private:
    ToricPosition position;
public:
    Positionable();
    Positionable(const ToricPosition& pos);
    ToricPosition getPosition() const;
    void setPosition(const ToricPosition& position);
    std::ostream& display(std::ostream& out) const;


	// A COMPLETER
};
std::ostream& operator<<(std::ostream&, Positionable const&);



