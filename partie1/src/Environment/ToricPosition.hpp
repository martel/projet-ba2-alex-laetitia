/*
 * POOSV 2020-21
 * @author:
 */

#pragma once
#include "Utility/Vec2d.hpp"

/*!
 * @brief Handle toric coordinate, distance and other
 * basic math operation in a toric world
 *
 * @note Gets the dimensions of the world from getAppConfig()
 */

class ToricPosition
{
private:
    Vec2d coord; //coordonnées dans le monde
    Vec2d dim_monde; //dimensions du monde torique dans lequel on se trouve
    void clamp();
public:
//    ToricPosition(const Vec2d& vector); //constructeur pour initializer les coord
//    ToricPosition(); // constructeur par défault --> plus efficace dans ToricPosition qui appelle des doubles
    ToricPosition(const Vec2d& vector_coor, const Vec2d& vector_dim);
    ToricPosition(const double& x = 0.0,const double& y = 0.0);
    ToricPosition(const Vec2d& vecteur);
//    ToricPosition();
    ToricPosition(const ToricPosition& a_copier) = default; //constructeur de copie already exists for each class by default, hence no need to redefine it.
    ToricPosition& operator=(const ToricPosition&) = default; //interne car on aura besoin d'acceder aux parties privés de la classe, mais existe déjà par défault.
    Vec2d const& toVec2d() const;
    double x() const; //the tests require getters with the name x() and y() to verify the values of the coord).
    double y() const; // the const is essential for the *this pointer to work, as it requires a const value.
    bool operator==(ToricPosition ) const;
    ToricPosition& operator+=(ToricPosition const&);
    double operator[](int index) const;
    Vec2d toricVector(ToricPosition const& that) const;



    // A COMPLETER
};

//externe:

std::ostream& operator<<(std::ostream&, ToricPosition const&);
ToricPosition operator+(ToricPosition, ToricPosition const&);
double toricDistance(ToricPosition const& from, ToricPosition const& to);

// FONCTION A COMPLETER
