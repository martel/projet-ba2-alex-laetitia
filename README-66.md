# Projet groupe 66

Pour acceder le code de source: https://gitlab.epfl.ch/martel/projet-ba2-alex-laetitia

Ce projet consiste en une simulation d'un environnement dans lequel sont modélisés les interractions entre différents animaux et les sources de nourritures.

Ce projet contient 2 types d'animaux:

- les fourmis, appartenant à une fourmilière et se déplaçant en suivant les traînées de phéromones qu'elles déposent en se déplaçant
- les termites, chassant les fourmis

Il existe 2 types de fourmis:

- soldats (antsoldiers), dessinées en rouge et attaquant les autres animaux n'appartenant pas à leur fourmilière
- ouvrières (antworker), dessinées en noir et dont le rôle est de trouver de la nourriture avant de la ramener jusqu'à leur fourmilière


Il existe 3 types d'objets inertes, les sources de nourritures (Food) et les fourmilières (AntHill), qui génèrent des fourmis et servent à stocker de la nourriture. Les phéromones sont déposées par les fourmis sur leur passage.



##COMPILATION ET EXÉCUTION:

Ce projet utilise  [Cmake](https://cmake.org/) pour compiler

* en ligne de commande :
    - dans le dossier build: cmake ../src
    - make nom_cible pour la génération de la cible

* Dans QTCreator:
       - mise en place du projet : ouvrir le fichier src/CmakeLists.txt
       - choisir la cible à exécuter


##CIBLES PRINCIPALES

- application : version finale du projet (visualisation de touts les animaux, fourmilières, phéromones et sources de nourriture et ajout d'un suivis statistique grâce à des graphes et chargement de configuration)
- antTest: évolution des fourmies uniquement
- anthillTest: évolution des fourmilières
- foodTest: génération de nourriture
- pheromoneTest: répandre et suivre des phéromones
- termiteTest: générer des termites en plus des fourmis et les faire se battre
- Tests unitaires sans affichage graphique


##COMMANDES

Les commandes d'une cible particulière sont indiquées dans une fenêtre d'aide en haut à droite de la simulation.
Elles permettent d'ajouter des objets (fourmilières, fourmis, termites) et de changer modes d'affichages (afficher les phéromones, mode debug, etc...)

##MODIFICATION DE CONCEPTION

L'entièreté de l'énoncé a été réalisée. Cependant, nous avons fait quelques ajouts:

- les sources de nourriture deviennent désormais toxiques après avoir été laissées à l'air libre au delà d'une certaine limite de temps 
- Une ouvrière meurt si elle transporte de la nourriture toxique durant une trop longue période
- si une ouvrière dépose de la nourriture toxique dans une fourmilière, son stock de nourriture est remis à zéro
- les anthelper, une nouvelle classe de fourmis. Elles sont dessinées en bleue et vont éliminer les sources de nourritures toxiques. Elles vont ignorer les autres animaux tout comme les antworker et ne pas engager de combat sauf si elles sont attaqué..Le cas échéant, elle "attaquent" sans faire de dégats.
- Ajout d'un "cycle du co2" dans lequel les animaux et les sources de nourritures toxiques en libère et les sources normales en absorbent
- la température est désormais dépendante de la quantité de co2 présente (on considère l'environnement comme une boîte de volume et pression fixe)


##FICHIER DE CONFIGURATION

app-origin.json est le fichier de configuration original. app.json contient des données en plus qui seront nécessaire au fonctionnement des extensions.
map1.map est le fichier utilisé pour charger une configuration particulière de l'environnement.



